﻿using AutoMapper;
using EducationApp.BusinessLogicLayer.Models.Account.EmailHelper;
using EducationApp.BusinessLogicLayer.Models.Author;
using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition;
using EducationApp.BusinessLogicLayer.Models.OrderItem;
using EducationApp.BusinessLogicLayer.Models.Pagination;
using EducationApp.BusinessLogicLayer.Models.PrintingEdition;
using EducationApp.BusinessLogicLayer.Models.StripePayment;
using EducationApp.BusinessLogicLayer.Models.User;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.DTO;
using Stripe;
using System;
using System.Linq;
using Order = EducationApp.DataAccessLayer.Entities.Order;
using OrderItem = EducationApp.DataAccessLayer.Entities.OrderItem;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class AutoMapperProfile : Profile 
    {
        public AutoMapperProfile()
        {
            CreateMap<PrintEditionModel, PrintingEdition>()
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => Convert.ToInt32(Math.Abs(src.Price) * 100)));
            CreateMap<PrintingEdition, PrintEditionModel>()
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => (Convert.ToDecimal(src.Price)) / 100))
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.AuthorInPrintingEditions.FirstOrDefault().Author.Id))
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src =>src.AuthorInPrintingEditions.FirstOrDefault().Author.Name));
            CreateMap<PrintEditionForEditModel, PrintingEdition>()
                .ForMember(dest => dest.AuthorInPrintingEditions, opt => opt.Ignore())
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => Convert.ToInt32(src.Price * 100)));
            CreateMap<PrintingEdition, PrintEditionForEditModel>()
                .ForMember(dest => dest.ResultMessage, opt => opt.Ignore())
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.AuthorInPrintingEditions.FirstOrDefault().Author.Id))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => (Convert.ToDecimal(src.Price)) / 100));
            CreateMap<PrintingEdition, PrintEditionFieldsModel>()
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => src.AuthorInPrintingEditions.FirstOrDefault().Author.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => (Convert.ToDecimal(src.Price)) / 100));

            CreateMap<AuthorDTO, AuthorFieldsModel>();
            CreateMap<Author, AuthorModel>();

            CreateMap<UserCreationModel, User>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
            CreateMap<User, UserForEditModel>()
                .ForMember(dest => dest.ExecutionResultMessage, opt => opt.Ignore())
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.ConfirmPassword, opt => opt.Ignore());
            CreateMap<User, UserFieldsModel>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
                .AfterMap((s, d) => d.Lockout = (s.LockoutEnd > DateTime.Now));
            CreateMap<User, UserForEditByAdminModel>();

            CreateMap<OrderItemModel, OrderItem>()
                .ForMember(dest => dest.PrintingEditionId, opt => opt.MapFrom(src => src.PrintEditionId)); 

            CreateMap<Order, OrderFieldsViewModel>()
                .ForMember(dest => dest.OrderTime, opt => opt.MapFrom(src => src.CreationData))
                .ForMember(dest => dest.OrderItemsList, opt => opt.MapFrom(src => src.OrderItems))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount / 100))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FirstName + src.User.LastName))
                .ForMember(dest => dest.PaymentTime, opt => opt.MapFrom(src => src.Payment.CreationData))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email));
            CreateMap<OrderItem, OrderItemViewModel>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.PrintingEdition.Title))
                .ForMember(dest => dest.UnitPrice , opt => opt.MapFrom(src => src.PrintingEdition.Price/100));
            CreateMap<PaymentRequestModel, OrderSaveRequestModel>();
            CreateMap<Charge, Payment>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.StripeChargeId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.StripeCustomerId, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.DateTimeCreatedUTC, opt => opt.MapFrom(src => src.Created));
            CreateMap<OrderListRequestModel, PageInfo>()
                .ForMember(dest => dest.PageNumber, opt => opt.MapFrom(src => src.Page))
                .AfterMap((s, d) => d.TotalPages = (int)Math.Ceiling((decimal)d.TotalItems / d.PageSize));
            CreateMap<UserRequestModel, PageInfo>()
                .ForMember(dest => dest.PageNumber, opt => opt.MapFrom(src => src.Page))
                .AfterMap((s, d) => d.TotalPages = (int)Math.Ceiling((decimal)d.TotalItems / d.PageSize));
            CreateMap<PrintEditionRequestModel,PageInfo>()
                .ForMember(dest => dest.PageNumber, opt => opt.MapFrom(src => src.Page))
                .AfterMap((s, d) => d.TotalPages = (int)Math.Ceiling((decimal)d.TotalItems / d.PageSize));
            CreateMap<AuthorRequestModel, PageInfo>()
                .ForMember(dest => dest.PageNumber, opt => opt.MapFrom(src => src.Page))
                .AfterMap((s, d) => d.TotalPages = (int)Math.Ceiling((decimal)d.TotalItems / d.PageSize));
            CreateMap<SignUpModel, User>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
            CreateMap<PaymentRequestModel, ChargeCreateOptions>()
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => Convert.ToInt32(src.Amount)))
                .ForMember(dest => dest.ReceiptEmail, opt => opt.MapFrom(src => src.StripeEmail))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.StripeToken));
            CreateMap<PrintEditionRequestModel, GetPrintEditionRequestDTO>()
                .ForMember(dest => dest.MinPrice, opt => opt.MapFrom(src => Convert.ToInt32(src.MinPrice * 100)))
                .ForMember(dest => dest.MaxPrice, opt => opt.MapFrom(src => Convert.ToInt32(src.MaxPrice * 100)));
        }
    }
}
