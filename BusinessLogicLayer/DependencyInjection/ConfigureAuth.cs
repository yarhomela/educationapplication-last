﻿using EducationApp.DataAccessLayer.Entities.Base;
using EducationApp.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EducationApp.BusinessLogicLayer.DependencyInjection
{
    public static class ConfigureAuth
    {
        public static void ConfigureIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            var defaultConection = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(defaultConection));
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationContext>()
                            .AddDefaultTokenProviders();
        }
    }
}
