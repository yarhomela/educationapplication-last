﻿using EducationApp.BusinessLogicLayer.Common;
using EducationApp.BusinessLogicLayer.Common.Interfaces;
using EducationApp.BusinessLogicLayer.Services;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Repositories.DapperRepositories;
using EducationApp.DataAccessLayer.Repositories.EFRepositories;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EducationApp.BusinessLogicLayer.DependencyInjection
{
    public static class DependencyInjection
    {
        public static void SetDependencies(this IServiceCollection services)
        {
            services.AddTransient<ILogger, Logger>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPrintingEditionService, PrintingEditionService>();
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPaymentService, PaymentService>();

            //--------------EFRepositories

            services.AddScoped<IUserRepository<User>, UserRepository>(); // UserManager
            //services.AddScoped<IPrintingEditionRepository<PrintingEdition>, PrintingEditionRepository>();
            //services.AddScoped<IAuthorRepository<Author>, AuthorRepository>();
            //services.AddScoped<IAuthorInPrintingEditionRepository<AuthorInPrintingEdition>, AuthorInPrintingEditionRepository>();
            //services.AddScoped<IPaymentRepository<Payment>, PaymentRepository>();
            //services.AddScoped<IOrderRepository<Order>, OrderRepository>();
            //services.AddScoped<IOrderItemRepository<OrderItem>, OrderItemRepository>();

            //----------Dapper repositories

            services.AddScoped<IAuthorRepository<Author>, AuthorDapperRepository>();
            services.AddScoped<IPrintingEditionRepository<PrintingEdition>, PrintingEditionDapperRepository>();
            services.AddScoped<IAuthorInPrintingEditionRepository<AuthorInPrintingEdition>, AuthorInPrintingEditionDapperRepository>();
            services.AddScoped<IPaymentRepository<Payment>, PaymentRepository>();
            services.AddScoped<IOrderRepository<Order>, OrderDapperRepository>();
            services.AddScoped<IOrderItemRepository<OrderItem>, OrderItemDapperRepository>();


        }

    }
}
