﻿using EducationApp.BusinessLogicLayer.Models.Account;
using EducationApp.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EducationApp.PresentationLayer.Helpers
{
    public class JwtHelper
    {
        UserManager<User> _userManager;
        public const string ISSUER = "MyAuthServer";
        public const string AUDIENCE = "http://localhost:64031/";
        const string KEY = "mysupersecret_secretkey!123";
        public const int LIFETIME = 60;
        public JwtHelper(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
        public async Task<string> CreateAccessToken(User user)
        {
            var identity = await GetClaimsForAccess(user);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: ISSUER,
                    audience: AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(LIFETIME)),
                    signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var accessToken = encodedJwt;
            return accessToken;
        }
        public string CreateRefreshToken(User user)
        {
            var identity = GetClaimsForRefresh(user);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: ISSUER,
                    audience: AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromDays(60)),
                    signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwt);
            return accessToken;
        }
        private async Task<ClaimsIdentity> GetClaimsForAccess(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            var role = roles.FirstOrDefault();
            var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Role, role),
                    new Claim(ClaimTypes.Name, user.UserName),
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
        private ClaimsIdentity GetClaimsForRefresh(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
            };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
        public GetClaimsFromTokenModel Refresh(RefreshTokenModel model)
        {
            var claimsModel = new GetClaimsFromTokenModel();
            claimsModel.Principal = GetPrincipalFromExpiredToken(model.AccessToken);
            JwtSecurityToken refreshToken = new JwtSecurityTokenHandler().ReadJwtToken(model.RefreshToken);
            if (refreshToken.ValidTo < DateTime.Now)
            {
                claimsModel.IsTokenValid = false;
            }
            claimsModel.IsTokenValid = true;
            return claimsModel;
        }
        private ClaimsPrincipal GetPrincipalFromExpiredToken(string accessToken)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {   
                ValidateIssuer = true,
                ValidIssuer = ISSUER,
                ValidateAudience = true,
                ValidAudience = AUDIENCE,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSymmetricSecurityKey(),
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(accessToken, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        }
    }
}
