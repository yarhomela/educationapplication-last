﻿using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using EducationApp.BusinessLogicLayer.Models.EmailHelper;
using Microsoft.Extensions.Options;

namespace EducationApp.BusinessLogicLayer.Helpers
{
    public class EmailHelper
    {
        private readonly SmtpClient _smtpClient;
        private readonly MailMessage _mailMessage;
        EmailSettingsModel _emailSetings;
        public EmailHelper( IOptions<EmailSettingsModel> options)
        {
            _smtpClient = new SmtpClient();
            _mailMessage = new MailMessage();
            _emailSetings = options.Value;
            _smtpClient.Host = _emailSetings.SmtpHost;
            _smtpClient.Port = _emailSetings.SmtpPort;
            _smtpClient.EnableSsl = _emailSetings.SmtpEnableSSL;
            _smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            _smtpClient.UseDefaultCredentials = false;
            _smtpClient.Credentials = new NetworkCredential(_emailSetings.EmailFrom, _emailSetings.PasswordFrom);
        }
        public async Task<bool> SendResetPasswordConfirm(string recipientEmail, string password)
        {
            _mailMessage.From = new MailAddress(_emailSetings.EmailFrom);
            _mailMessage.To.Add(recipientEmail);
            _mailMessage.Subject = "Confirm password reset";
            _mailMessage.Body = "Your new password:"+password;
            await _smtpClient.SendMailAsync(_mailMessage);
            return true;
        }
        public async Task<bool> SendEmailConfirm(string recipientEmail)
        {
            _mailMessage.From = new MailAddress(_emailSetings.EmailFrom);
            _mailMessage.To.Add(recipientEmail);
            _mailMessage.Subject = "Confirm email";
            _mailMessage.Body = "Please confirm your email";
            await _smtpClient.SendMailAsync(_mailMessage);
            return true;
        }
    }
}
