﻿using System;

namespace EducationApp.PresentationLayer.Helpers
{
    public static class PasswordGenerator
    {
        public static string GenerateValidPassword()
        {
            string password = "";
            char[] letters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            Random random = new Random();
            for (int i=0;i<5;i++)
            {
                var index = random.Next(letters.Length);
                password += letters[index];
            }
            char[] LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            for (int i = 0; i < 5; i++)
            {
                var index = random.Next(LETTERS.Length);
                password += LETTERS[index];
            }
            char[] characters = "+-*#$%&".ToCharArray();
            for (int i = 0; i < 3; i++)
            {
                var index = random.Next(characters.Length);
                password += characters[index];
            }
            char[] numbers = "0123456789".ToCharArray();
            for (int i = 0; i < 5; i++)
            {
                var index = random.Next(numbers.Length);
                password += numbers[index];
            }
            return password;
        }
    }
}
