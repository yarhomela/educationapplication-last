﻿
namespace EducationApp.BusinessLogicLayer.Models.Stripe
{
    public class StripeSettingsModel
    {
        public string SecretKey { get; set; }
    }
}
