﻿
namespace EducationApp.BusinessLogicLayer.Models.EmailHelper
{
    public class EmailSettingsModel
    {
        public string EmailFrom { get; set; } 
        public string PasswordFrom { get; set; } 
        public string SmtpHost { get; set; } 
        public int SmtpPort { get; set; } 
        public bool SmtpEnableSSL { get; set; } 
    }
}
