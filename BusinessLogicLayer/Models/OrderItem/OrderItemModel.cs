﻿
using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.BusinessLogicLayer.Models.Order
{
    public class OrderItemModel
    {
        public string PrintEditionId { get; set; }
        public int Quantity { get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
    }
}
