﻿using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.BusinessLogicLayer.Models.OrderItem
{
    public class OrderItemViewModel
    {
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string PrintingEditionId { get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
    }
}
