﻿using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class AuthorFieldsModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> Products { get; set; }
    }
}
