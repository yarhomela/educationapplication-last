﻿
using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class AuthorRequestModel
    {
        [Required]
        [Range(0,Int32.MaxValue)]
        public int Page { get; set; }
        public int PageSize { get; set; }
        public AuthorSorting AuthorSorting { get; set; }
    }
}
