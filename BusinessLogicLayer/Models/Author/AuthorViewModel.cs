﻿using EducationApp.BusinessLogicLayer.Models.Pagination;
using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class AuthorViewModel
    {
        public List<AuthorFieldsModel> AuthorFields { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
