﻿
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class EditAuthorModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string EditResultMessage { get; set; }
        public bool IsEdited { get; set; }
    }
}
