﻿
namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class RemoveAuthorResponseModel
    {
        public bool IsRemoved { get; set; }
        public string ResultMessage { get; set; }
    }
}
