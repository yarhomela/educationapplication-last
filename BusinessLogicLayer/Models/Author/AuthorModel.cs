﻿
namespace EducationApp.BusinessLogicLayer.Models.Author
{
    public class AuthorModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
