﻿
using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.StripePayment
{
    public class PaymentRequestModel
    {
        public List<OrderItemModel> OrderItemModels { get; set; }
        public string Description { get; set; }
        public string OrderId { get; set; }
        [Required]
        public string StripeToken { get; set; }
        [Required]
        [EmailAddress]
        public string StripeEmail { get; set; }
        public decimal Amount { get; set; }
        public Currency Currency { get; set; }
        
    }
}
