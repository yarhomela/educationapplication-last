﻿
namespace EducationApp.BusinessLogicLayer.Models.Payment
{
    public class PaymentResponseModel
    {
        public string ExecutionResultMessage { get; set; }
        public bool IsSuccess { get; set; }
    }
}
