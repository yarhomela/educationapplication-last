﻿
namespace EducationApp.BusinessLogicLayer.Models.Account
{
    public class SignUpResponseModel
    {
        public string ResultMessage { get; set; }
        public bool IsSignUp { get; set; }
    }
}
