﻿
namespace EducationApp.BusinessLogicLayer.Models.Account
{
    public class ResponseTokenModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string UserName { get; set; }
        public string ExecutionResultMessage { get; set; }
        public string UserRole { get; set; }
        public bool IsAuth { get; set; }
    }
}
