﻿
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.Account
{
    public class RefreshTokenModel
    {
        [Required]
        public string AccessToken { get; set; }
        [Required]
        public string RefreshToken { get; set; }
        public string ExecutionResultMessage { get; set; }
        public bool IsRefresh { get; set; }
    }
}
