﻿
namespace EducationApp.BusinessLogicLayer.Models.Account
{
    public class ForgotPasswordResponseModel
    {
        public string ResultMessage { get; set; }
        public bool IsExistingUser { get; set; }
    }
}
