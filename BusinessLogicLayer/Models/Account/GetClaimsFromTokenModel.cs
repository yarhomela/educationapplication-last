﻿using System.Security.Claims;

namespace EducationApp.BusinessLogicLayer.Models.Account
{
    public class GetClaimsFromTokenModel
    {
        public ClaimsPrincipal Principal { get; set; }
        public bool IsTokenValid { get; set; }
    }
}
