﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.Order
{
    public class OrderListRequestModel
    {
        [Required]
        [Range(0, Int32.MaxValue)]
        public int Page { get; set; }
        public int PageSize { get; set; }
        public Status Status { get; set; }
        public OrderSorting OrderSorting { get; set; }
    } 
}
