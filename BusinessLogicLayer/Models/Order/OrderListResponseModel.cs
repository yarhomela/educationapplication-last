﻿using EducationApp.BusinessLogicLayer.Models.Pagination;
using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.Order
{
    public class OrderListResponseModel
    {
        public List<OrderFieldsViewModel> OrderFieldsList { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
