﻿using EducationApp.DataAccessLayer.Entities.Enums;
using Stripe;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition
{
    public class OrderSaveRequestModel
    {
        [Required]
        public List<OrderItemModel> OrderItemModels { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(0, Int32.MaxValue)]
        public decimal Amount { get; set; }
        [Required]
        [Range(0, 6)]
        public Currency Currency { get; set; }
        public Charge Charge { get; set; }
        public decimal AmountRest { get; set; }
    }
}
