﻿namespace EducationApp.BusinessLogicLayer.Models.Order
{
    public class OrderSaveResponseModel
    {
        public string ExecutionResultMessage { get; set; }
        public bool IsSaved { get; set; }
        public string OrderId { get; set; }
    }
}
