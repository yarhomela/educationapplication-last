﻿using EducationApp.BusinessLogicLayer.Models.OrderItem;
using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.Order
{
    public class OrderFieldsViewModel
    {
        public string Id { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime PaymentTime { get; set; }
        public List<OrderItemViewModel> OrderItemsList { get; set; }
        public Currency Currency { get; set; }
        public decimal Amount { get; set; }
        public Status Status { get; set; }
        public string PaymentId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
