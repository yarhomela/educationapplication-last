﻿
using EducationApp.BusinessLogicLayer.Models.Pagination;
using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class UserViewModel
    {
        public List<UserFieldsModel> UserFieldsModels { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
