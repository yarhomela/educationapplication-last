﻿
namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class UserFieldsModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool Lockout { get; set; }
    }
}
