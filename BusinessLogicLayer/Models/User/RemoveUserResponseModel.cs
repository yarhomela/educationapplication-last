﻿
namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class RemoveUserResponseModel
    {
        public bool IsRemoved { get; set; }
        public string ResultMessage { get; set; }
    }
}
