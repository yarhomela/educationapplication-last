﻿
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class UserForEditModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string ExecutionResultMessage { get; set; }
    }
}
