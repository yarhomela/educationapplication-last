﻿
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class UserForEditByAdminModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public bool IsEdited { get; set; }
        public string ResultMessage { get; set; }
    }
}
