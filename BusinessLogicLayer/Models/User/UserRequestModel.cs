﻿
using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.User
{
    public class UserRequestModel
    {
        [Required]
        [Range(0,Int32.MaxValue)]
        public int Page { get; set; }
        public int PageSize { get; set; }
        public UserSorting UserSorting { get; set; }
    }
}
