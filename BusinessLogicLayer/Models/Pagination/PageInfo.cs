﻿using System;


namespace EducationApp.BusinessLogicLayer.Models.Pagination
{
    public class PageInfo
    {
        public int PageNumber { get; set; } 
        public int PageSize { get; set; } 
        public int TotalItems { get; set; } 
        public int TotalPages { get; set; }
    }
}
