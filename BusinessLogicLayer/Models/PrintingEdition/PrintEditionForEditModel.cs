﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class PrintEditionForEditModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Range(0,Int32.MaxValue)]
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
        [Required]
        public TypePrintEdit TypePrintEdit { get; set; }
        [Required]
        public string AuthorId { get; set; }
        public string ResultMessage { get; set; }
        public bool IsUpdated { get; set; }
    }
}
