﻿
using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class PrintEditionFieldsModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
        public string AuthorName { get; set; }
        public Currency Currency { get; set; }
        public decimal Price { get; set; }
    }
}
