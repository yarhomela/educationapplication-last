﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class PrintEditionModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        [StringLength(1000, MinimumLength = 3)]
        public string Description { get; set; }
        [Required]
        [Range(0,Int32.MaxValue)]
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
        [Required]
        public TypePrintEdit TypePrintEdit { get; set; }
        public string AuthorName { get; set; }
        [Required]
        public string AuthorId { get; set; }
    }
}
