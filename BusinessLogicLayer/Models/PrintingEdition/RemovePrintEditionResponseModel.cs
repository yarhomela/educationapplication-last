﻿
namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class RemovePrintEditionResponseModel
    {
        public bool IsRemoved { get; set; }
        public string ResultMessage { get; set; }
    }
}
