﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class PrintEditionRequestModel
    {
        [Required]
        [Range(0, Int32.MaxValue)]
        public int Page { get; set; }
        public int PageSize { get; set; }
        public PrintSorting PrintSorting { get; set; } 
        public TypePrintEdit TypePrintEdit { get; set; }
        public string SearchByTitle { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}
