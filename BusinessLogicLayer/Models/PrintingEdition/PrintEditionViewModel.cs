﻿using EducationApp.BusinessLogicLayer.Models.Pagination;
using System.Collections.Generic;

namespace EducationApp.BusinessLogicLayer.Models.PrintingEdition
{
    public class PrintEditionViewModel
    {
        public List<PrintEditionFieldsModel> PrintingEditionFieldsModels { get; set; }
        public PageInfo PageInfo { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}
