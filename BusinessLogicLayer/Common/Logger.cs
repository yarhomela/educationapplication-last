﻿using EducationApp.BusinessLogicLayer.Common.Interfaces;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Common
{
    public class Logger : ILogger
    {
        public async Task LogToFileAsync(Exception exception)
        {
            var exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string filePatch = $@"{exePath}\Logs";
            string fileName = $@"{DateTime.UtcNow.ToString("dd-MM-yyyy")}-log.txt";
            string fullPatch = $@"{filePatch}\{fileName}";
            Directory.CreateDirectory(filePatch);
            string streamWriterText =
                "-------------------------------" +
                $"Message:{exception.Message}" +
                $"Stack Trace:\n{exception.StackTrace}" +
                $"Request: {exception.InnerException} . Source: {exception.Source}" +
                "-------------------------------";
            using (StreamWriter streamWriter = File.AppendText(fullPatch))
            {
                await streamWriter.WriteLineAsync(streamWriterText);
            }
        }
    }
}
