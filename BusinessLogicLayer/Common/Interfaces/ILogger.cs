﻿using System;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Common.Interfaces
{
    public interface ILogger
    {
        Task LogToFileAsync(Exception exception);
    }
}
