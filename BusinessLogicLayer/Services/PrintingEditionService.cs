﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using EducationApp.BusinessLogicLayer.Models.PrintingEdition;
using System.Linq;
using System.Collections.Generic;
using EducationApp.BusinessLogicLayer.Models.Pagination;
using AutoMapper;
using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        IPrintingEditionRepository<PrintingEdition> _printingEditionRepository;
        IAuthorInPrintingEditionRepository<AuthorInPrintingEdition> _authorInPrintingEditionRepository;
        private readonly IMapper _mapper;

        public PrintingEditionService(IPrintingEditionRepository<PrintingEdition> printingEditionRepository,
            IAuthorInPrintingEditionRepository<AuthorInPrintingEdition> authorInPrintingEditionRepository, IMapper mapper)
        {
            _printingEditionRepository = printingEditionRepository;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _mapper = mapper;
        }

        public async Task<string> Create(PrintEditionModel model)
        {
            var printEdition = _mapper.Map<PrintingEdition>(model);
            printEdition.Currency = Currency.USD;
            var isPrintCreated = await _printingEditionRepository.CreateAsync(printEdition);
            var authorInPrint = new AuthorInPrintingEdition { AuthorId = model.AuthorId, PrintingEditionId = printEdition.Id,};
            var isAuthorCreated = await _authorInPrintingEditionRepository.CreateAsync(authorInPrint);
            if (isPrintCreated && isAuthorCreated)
            {
                return $"Printing edition {printEdition.Title} added";
            }
            return "Add printing edition failed";
        }
        public async Task<PrintEditionModel> Read(string printId)
        {
            var printEdition = await _printingEditionRepository.FindWithAuthor(printId);
            if(printEdition==null)
            {
                return null;
            }
            var responseModel = _mapper.Map<PrintEditionModel>(printEdition);
            return responseModel;
        }
        public async Task<PrintEditionForEditModel> Edit(PrintEditionForEditModel requestModel)
        {
            var responseModel = new PrintEditionForEditModel();
            var printEdition = await _printingEditionRepository.FindWithAuthor(requestModel.Id);
            if (printEdition == null)
            {
                responseModel.ResultMessage = "Printing edition not found";
                return responseModel;
            }
            var convertPriceToInt = 100;
            if ((printEdition.Title != requestModel.Title)
                || (printEdition.Description != requestModel.Description)
                || (printEdition.Price != requestModel.Price* convertPriceToInt)
                || (printEdition.TypePrintEdit != requestModel.TypePrintEdit)
                || (printEdition.AuthorInPrintingEditions.FirstOrDefault().Author.Id != requestModel.AuthorId))
            {
                printEdition = _mapper.Map(requestModel, printEdition);
                printEdition.AuthorInPrintingEditions.FirstOrDefault().AuthorId = requestModel.AuthorId;
                printEdition.AuthorInPrintingEditions.FirstOrDefault().Author = null;
                var isUpdated = await _printingEditionRepository.UpdateWithAuthor(printEdition);
                responseModel = requestModel;
                responseModel.ResultMessage = $"Printing edition updated - {isUpdated}";
                responseModel.IsUpdated = isUpdated;
                return responseModel;
            }
            responseModel = requestModel;
            responseModel.IsUpdated = false;
            responseModel.ResultMessage = "Printing edition not updated";
            return responseModel;
        }

        public async Task<PrintEditionViewModel> GetPrintEditions(PrintEditionRequestModel requestModel)
        {
            PrintEditionViewModel printEditionsModel = new PrintEditionViewModel();
            if (requestModel.PageSize <= 0)
            {
                requestModel.PageSize = 6;
            }
            printEditionsModel.PrintingEditionFieldsModels = new List<PrintEditionFieldsModel>();
            var requestDTO = _mapper.Map<GetPrintEditionRequestDTO>(requestModel);
            var response = await _printingEditionRepository.GetPrintEditionList(requestDTO);
            var printEditionsList = response.PrintingEditions;
            var totalItems = response.TotalItems;
            int convertRealValue = 100;
            printEditionsModel.MinPrice = response.MinPrice/ convertRealValue;
            printEditionsModel.MaxPrice = response.MaxPrice/ convertRealValue;
            printEditionsModel.PrintingEditionFieldsModels = _mapper.Map(printEditionsList, printEditionsModel.PrintingEditionFieldsModels);
            var pageInfo = new PageInfo() { TotalItems = totalItems };
            pageInfo = _mapper.Map(requestModel, pageInfo);
            printEditionsModel.PageInfo = pageInfo;
            return printEditionsModel;
        }
        public async Task<RemovePrintEditionResponseModel> Remove(string printID)
        {
            var responseModel = new RemovePrintEditionResponseModel();
            responseModel.IsRemoved = false;
            var printEdition = await _printingEditionRepository.FindWithAuthor(printID);
            if (printEdition == null)
            {
                responseModel.ResultMessage = "Print edition not found";
                return responseModel;
            }
            if (printEdition.IsRemoved == true)
            {
                responseModel.ResultMessage = "Print edition is already removed";
                return responseModel;
            }
            printEdition.IsRemoved = true;
            if (printEdition.AuthorInPrintingEditions != null)
            {
                printEdition.AuthorInPrintingEditions.FirstOrDefault().IsRemoved = true;
            }
            var isUpdated = await _printingEditionRepository.UpdateWithAuthor(printEdition);
            if (isUpdated)
            {
                responseModel.ResultMessage = $"Printing edition: {printEdition.Title} removed";
                responseModel.IsRemoved = true;
                return responseModel;
            }
            responseModel.ResultMessage = "Remove printing edition failed";
            return responseModel;
        }
        public async Task<string> FullRemove (string printEditionId)
        {
            var isFullRemoved = await _printingEditionRepository.RemoveAsync(printEditionId);
            if(isFullRemoved)
            {
                return "Print edition removed";
            }
            return "Remove print edition failed";
        }
    }
    
}
