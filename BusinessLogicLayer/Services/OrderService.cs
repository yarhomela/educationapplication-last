﻿using AutoMapper;
using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition;
using EducationApp.BusinessLogicLayer.Models.Pagination;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        IOrderRepository<Order> _orderRepository;
        IOrderItemRepository<OrderItem> _orderItemRepository;
        UserManager<User> _userManager;
        private readonly IMapper _mapper;
        public OrderService( IMapper mapper, IOrderRepository<Order> orderRepository, UserManager<User> userManager, IOrderItemRepository<OrderItem> orderItemRepository)
        {
            _orderRepository = orderRepository;
            _userManager = userManager;
            _orderItemRepository = orderItemRepository;
            _mapper = mapper;
        }
        public async Task<OrderSaveResponseModel> Save(OrderSaveRequestModel orderSaveModel, string userName)
        {
            var saveResponseModel = new OrderSaveResponseModel();
            var user = await _userManager.FindByNameAsync(userName);
            var order = new Order {
                Description = orderSaveModel.Description,
                Amount = Convert.ToInt32(orderSaveModel.Amount),
                Status = Status.Unpaid,
                Currency = orderSaveModel.Currency,
                UserId = user.Id
            };
            var orderItems = new List<OrderItem>();
            foreach (var item in orderSaveModel.OrderItemModels)
            {
                var orderItem = _mapper.Map<OrderItem>(item);
                orderItem.OrderId = order.Id;
                orderItems.Add(orderItem);
            }
            order.OrderItems = orderItems;
            if (orderSaveModel.Charge !=null)
            {
                order.Status = Status.Paid;
            }
            if(orderSaveModel.AmountRest != 0)
            {
                order.Status = Status.Unpaid;
                order.Amount = Convert.ToInt32(orderSaveModel.AmountRest *100);
            }
            var isOrderCreated = await _orderRepository.CreateWithOrderItems(order);
            if (!isOrderCreated)
            {
                saveResponseModel.ExecutionResultMessage = "Error creating order";
                return saveResponseModel;
            }
            saveResponseModel.ExecutionResultMessage = "Save order successful";
            saveResponseModel.IsSaved = true;
            saveResponseModel.OrderId = order.Id;
            return saveResponseModel;
        }
        public async Task<OrderListResponseModel> GetOrders(OrderListRequestModel requestModel, string userName, bool isAdmin)
        {
            var responseModel = new OrderListResponseModel();
            responseModel.OrderFieldsList = new List<OrderFieldsViewModel>();
            var user = await _userManager.FindByNameAsync(userName);
            requestModel.PageSize = 6;
            var response = await _orderRepository.GetOrderList(requestModel.Page, requestModel.PageSize, user, isAdmin, requestModel.Status, requestModel.OrderSorting);
            var orderList = response.Item1;
            var totalItems = response.totalItems;
            responseModel.OrderFieldsList = _mapper.Map(orderList, responseModel.OrderFieldsList);
            var pageInfo = new PageInfo() { TotalItems = totalItems };
            pageInfo = _mapper.Map(requestModel, pageInfo);
            responseModel.PageInfo = pageInfo;
            return responseModel;
        }
        public async Task<string> FullRemove(string orderId)
        {
            var isRemoved = await _orderRepository.RemoveAsync(orderId);
            if (isRemoved)
            {
                return "Order is removed";
            }
            return "Remove failed";
        }
        public async Task<string> FullRemoveOrderItem(string orderItemId)
        {
            var isRemoved = await _orderItemRepository.RemoveAsync(orderItemId);
            if (isRemoved)
            {
                return "Order item is removed";
            }
            return "Remove failed";
        }
    }
}
