﻿using AutoMapper;
using EducationApp.BusinessLogicLayer.Helpers;
using EducationApp.BusinessLogicLayer.Models.Account;
using EducationApp.BusinessLogicLayer.Models.Account.EmailHelper;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using EducationApp.PresentationLayer.Helpers;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        EmailHelper _emailHelper;
        IUserRepository<User> _userRepository;
        UserManager<User> _userManager;
        SignInManager<User> _signInManager;
        JwtHelper _jwtHelper;
        private readonly IMapper _mapper;

        public AccountService(EmailHelper emailHelper, IUserRepository<User> userRepository, UserManager<User> userManager, SignInManager<User> signInManager,
            JwtHelper jwtHelper,IMapper mapper)
        {
            _userRepository = userRepository;
            _emailHelper = emailHelper;
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtHelper = jwtHelper;
            _mapper = mapper;
        }

        public async Task<SignUpResponseModel> SignUp(SignUpModel model)
        {
            var responseModel = new SignUpResponseModel();
            var user = _mapper.Map<User>(model);
            var userExist = await _userManager.FindByEmailAsync(model.Email);
            if(userExist != null)
            {
                responseModel.ResultMessage = "User is already exist. Change your input email";
                return responseModel;
            }
            var create = await _userManager.CreateAsync(user, model.Password);
            if (!create.Succeeded)
            {
                responseModel.ResultMessage = "User not created";
                return responseModel;
            }
            try
            {
                var sending = await _emailHelper.SendEmailConfirm(model.Email);
                if (!sending)
                {
                    responseModel.ResultMessage = "Send message is failed";
                    return responseModel;
                }
            }
            catch (System.Exception)
            {
                responseModel.ResultMessage = "Error send confirm message. We apologize";
                return responseModel;
            }
            var confirm = await _userRepository.ConfirmEmail(user);
            if (!confirm)
            {
                responseModel.ResultMessage = "Confirm error";
                return responseModel;
            }
            var add = await _userManager.AddToRoleAsync(user, "user");
            if (!add.Succeeded)
            {
                responseModel.ResultMessage = "Add to role failed";
                return responseModel;
            }
            responseModel.ResultMessage = "Sign-Up successful";
            responseModel.IsSignUp = true;
            return responseModel;
        }
        public async Task<ForgotPasswordResponseModel> ForgotPassword(string email)
        {
            var responseModel = new ForgotPasswordResponseModel();
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                responseModel.ResultMessage = "User not found";
                responseModel.IsExistingUser = false;
                return responseModel;
            }
            responseModel.IsExistingUser = true;
            var password = PasswordGenerator.GenerateValidPassword();
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var isPassChanged = await _userManager.ResetPasswordAsync(user, token, password);
            if(!isPassChanged.Succeeded)
            {
                responseModel.ResultMessage = "Reset password failed";
                return responseModel;
            }
            try
            {
                var sending = await _emailHelper.SendResetPasswordConfirm(email, password);
                if (!sending)
                {
                    responseModel.ResultMessage = "Send message is failed";
                    return responseModel;
                }
            }
            catch (System.Exception)
            {
                responseModel.ResultMessage = "Error send new password on your email. We apologize";
                return responseModel;
            }
            
            responseModel.ResultMessage = "Reset password complete";
            return responseModel;
        }
        public async Task<ResponseTokenModel> SignIn(SignInModel model)
        {
            var responseModel = new ResponseTokenModel();
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                responseModel.ExecutionResultMessage = "User does not exist";
                return responseModel;
            }
            if(user.IsRemoved == true)
            {
                responseModel.ExecutionResultMessage = "User removed";
                return responseModel;
            }
            var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.IsPersistent, user.LockoutEnabled);
            if (!result.Succeeded)
            {
                responseModel.ExecutionResultMessage =  $"{result}";
                return responseModel;
            }
            await _signInManager.SignInAsync(user, model.IsPersistent);
            responseModel.AccessToken = await _jwtHelper.CreateAccessToken(user);
            responseModel.RefreshToken = _jwtHelper.CreateRefreshToken(user);
            var userRoles = await _userManager.GetRolesAsync(user);
            responseModel.UserRole = userRoles.FirstOrDefault();
            responseModel.UserName = user.FirstName + " " + user.LastName;
            if(responseModel.AccessToken ==null)
            {
                responseModel.ExecutionResultMessage = "Create access token failed";
            }
            responseModel.ExecutionResultMessage = "Sign-In successfully";
            responseModel.IsAuth = true;
            return responseModel;
        }
        public string SignOut()
        {
            var user = _signInManager.SignOutAsync();
            if(user == null)
            {
                return "Sign out error";
            }
            return "Sign out is succeeded";
        }
        public async Task<RefreshTokenModel> Refresh (RefreshTokenModel model)
        {
            var responseModel = new RefreshTokenModel();
            var claimsModel = _jwtHelper.Refresh(model);
            var principal = claimsModel.Principal;
            var user = await _userManager.FindByNameAsync(principal.Identity.Name);
            if(user ==null)
            {
                responseModel.ExecutionResultMessage = "Refresh error: not found user claims from token. Please, Sign-In.";
                return responseModel;
            }
            if(user.LockoutEnd > DateTime.Now)
            {
                responseModel.ExecutionResultMessage = "Refresh error: user locked.";
                return responseModel;
            }
            var isValid = claimsModel.IsTokenValid;
            if(isValid)
            {
                responseModel.AccessToken = await _jwtHelper.CreateAccessToken(user);
                responseModel.RefreshToken = _jwtHelper.CreateRefreshToken(user);
                responseModel.ExecutionResultMessage = "Refresh successful. Tokens is updated";
                responseModel.IsRefresh = true;
                return responseModel;
            }
            responseModel.AccessToken = model.AccessToken;
            responseModel.RefreshToken = model.RefreshToken;
            responseModel.ExecutionResultMessage = "Refresh successful";
            return responseModel;
        }
    }
}
