﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using EducationApp.BusinessLogicLayer.Models.Pagination;
using EducationApp.BusinessLogicLayer.Models.Author;
using AutoMapper;
using EducationApp.DataAccessLayer.Repositories.Interfaces;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository<Author> _authorRepository;
        private readonly IMapper _mapper;
        public AuthorService(IAuthorRepository<Author> authorRepository, IMapper mapper)
        {
            _authorRepository = authorRepository;
            _mapper = mapper;
        }
        public async Task<string> Create(string authorName)
        {
            var isAuthorExist = await _authorRepository.FindByNameAsync(authorName);
            if(isAuthorExist!=null)
            {
                return "Author is already exist";
            }
            var author = new Author { Name = authorName };
            var isCreated = await _authorRepository.CreateAsync(author);
            if (isCreated)
            {
                return "Author added";
            }
            return "Add author failed ";
        }
        public async Task<string> GetAuthor(string authorID)
        {
            var author = await _authorRepository.FindByIdAsync(authorID);
            if(author ==null)
            {
                return "Author not found";
            }
            return author.Name;
        }
        public async Task<EditAuthorModel> Edit(EditAuthorModel editModel)
        {
            var author = await _authorRepository.FindByIdAsync(editModel.Id);
            if (author == null)
            {
                editModel.EditResultMessage = "Author not found";
                editModel.IsEdited = false;
                return editModel;
            }
            if (author.Name != editModel.Name)
            {
                author.Name = editModel.Name;
                var isEdited =  await _authorRepository.UpdateAsync(author);
                editModel.IsEdited = isEdited;
                editModel.EditResultMessage = $"Author {author.Name} edit - {isEdited}";
                return editModel;
            }
            editModel.EditResultMessage = "Author not edited";
            return editModel;
        }
        public async Task<RemoveAuthorResponseModel> Remove(string id)
        {
            var responseModel = new RemoveAuthorResponseModel();
            var author = await _authorRepository.FindByIdAsync(id);
            if (author == null)
            {
                responseModel.ResultMessage = "Author not found";
                return responseModel;
            }
            author.IsRemoved = true;
            var isUpdated = await _authorRepository.UpdateAsync(author);
            if (isUpdated)
            {
                responseModel.ResultMessage = $"Author {author.Name} removed";
                responseModel.IsRemoved = true;
                return responseModel;
            }
            responseModel.ResultMessage = "Remove author failed ";
            return responseModel;
        }
        public async Task<AuthorViewModel> GetAuthorsWithPrintEdition(AuthorRequestModel requestModel)
        {
            AuthorViewModel viewModel = new AuthorViewModel();
            viewModel.AuthorFields = new List<AuthorFieldsModel>();
            if(requestModel.PageSize <= 0)
            {
                requestModel.PageSize = 6;
            }
            var responseDTO = await _authorRepository.GetAuthorsList(requestModel.Page, requestModel.PageSize, requestModel.AuthorSorting);
            var authorsList = responseDTO.Authors;
            var totalItems = responseDTO.TotalItems;
            viewModel.AuthorFields = _mapper.Map(authorsList, viewModel.AuthorFields);
            var pageInfo = new PageInfo() { TotalItems = totalItems };
            pageInfo = _mapper.Map(requestModel, pageInfo);
            viewModel.PageInfo = pageInfo;
            return viewModel;
        }
        public async Task<string> FullRemove(string id)
        {
            var isRemoved = await _authorRepository.RemoveAsync(id);
            if (isRemoved)
            {
                return "Author removed";
            }
            return "Remove author failed ";
        }
        public async Task<List<AuthorModel>> GetAuthors()
        {
            var authorList = await _authorRepository.GetAllAuthors();
            var responseList = new List<AuthorModel>();
            responseList = _mapper.Map(authorList, responseList);
            return responseList;
        }
    }
}
