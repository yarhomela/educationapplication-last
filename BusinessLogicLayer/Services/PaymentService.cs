﻿using AutoMapper;
using EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition;
using EducationApp.BusinessLogicLayer.Models.Payment;
using EducationApp.BusinessLogicLayer.Models.Stripe;
using EducationApp.BusinessLogicLayer.Models.StripePayment;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.Options;
using Stripe;
using System;
using System.Threading.Tasks;
using Order = EducationApp.DataAccessLayer.Entities.Order;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class PaymentService : IPaymentService
    {
        IPaymentRepository<Payment> _paymentRepository;
        IOrderRepository<Order> _orderRepository;
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        public PaymentService(IOptions<StripeSettingsModel> options, IPaymentRepository<Payment> paymentRepository,
            IOrderRepository<Order> orderRepository, IOrderService orderService, IMapper mapper)
        {
            StripeConfiguration.ApiKey = options.Value.SecretKey;
            _paymentRepository = paymentRepository;
            _orderRepository = orderRepository;
            _orderService = orderService;
            _mapper = mapper;
        }
        public async Task<PaymentResponseModel> PayAndSave(PaymentRequestModel model, string userName)
        {
            var responseModel = new PaymentResponseModel();
            var options = _mapper.Map<ChargeCreateOptions>(model);
            options.Currency = model.Currency.ToString();
            try
            {
                var service = new ChargeService();
                Charge charge = service.Create(options);
                var orderSaveModel = _mapper.Map<OrderSaveRequestModel>(model);
                int allowableUnderpayment = 5;
                if (!((model.Amount - charge.Amount) < allowableUnderpayment))
                {
                    orderSaveModel.AmountRest = model.Amount - charge.Amount;
                }
                orderSaveModel.Charge = charge;
                var orderSaveResponse = await _orderService.Save(orderSaveModel, userName);
                if (!orderSaveResponse.IsSaved)
                {
                    responseModel.ExecutionResultMessage = orderSaveResponse.ExecutionResultMessage;
                    return responseModel;
                }
                var payment = _mapper.Map<Payment>(charge);
                payment.OrderId = orderSaveResponse.OrderId;
                var isPaymentCreated = await _paymentRepository.CreateAsync(payment);
                if (!isPaymentCreated)
                {
                    responseModel.ExecutionResultMessage = "Error created payment";
                    return responseModel;
                }
                responseModel.ExecutionResultMessage = "Pay and save order successful";
                responseModel.IsSuccess = true;
                return responseModel;
            }
            catch (Exception exc)
            {
                responseModel.ExecutionResultMessage = exc.Message;
                responseModel.IsSuccess = false;
                return responseModel;
            }
        }
        public async Task<PaymentResponseModel> Pay(PaymentRequestModel model, string userName)
        {
            var paymentResponse = new PaymentResponseModel();
            var options = _mapper.Map<ChargeCreateOptions>(model);
            options.Currency = model.Currency.ToString();
            try
            {
                var service = new ChargeService();
                Charge charge = service.Create(options);

                var order = await _orderRepository.FindByIdAsync(model.OrderId);
                order.Status = Status.Paid;
                order.Currency = model.Currency;
                var payment = _mapper.Map<Payment>(charge);
                payment.OrderId = order.Id;
                int allowableUnderpayment = 5;
                if (!((model.Amount - charge.Amount) < allowableUnderpayment))
                {
                    var amountRest = model.Amount - charge.Amount;
                    int valuePreservationMultiplier = 100;
                    order.Amount = Convert.ToInt32(amountRest * valuePreservationMultiplier);
                    order.Status = Status.Unpaid;
                    paymentResponse.ExecutionResultMessage = "Pay isn't full. Pay another " + amountRest.ToString();
                }
                var isPaymentCreated = await _paymentRepository.CreateAsync(payment);
                if (!isPaymentCreated)
                {
                    paymentResponse.ExecutionResultMessage = "Error created payment";
                    return paymentResponse;
                }
                var isOrderSaved = await _orderRepository.SecureUpdate(order);       
                if (isOrderSaved)
                {
                    paymentResponse.ExecutionResultMessage = "Payment successful";
                    paymentResponse.IsSuccess = true;
                    return paymentResponse;
                }
                paymentResponse.ExecutionResultMessage = "Pay failed";
                return paymentResponse;
            }
            catch (Exception exc)
            {
                paymentResponse.ExecutionResultMessage = exc.Message;
                paymentResponse.IsSuccess = false;
                return paymentResponse;
            }
        }
        public async Task<string> FullRemove(string paymentId)
        {
            var isRemoved = await _paymentRepository.RemoveAsync(paymentId);
            if (isRemoved)
            {
                return "Payment is removed";
            }
            return "Payment failed";
        }
    }
}
