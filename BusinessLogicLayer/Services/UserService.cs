﻿using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using EducationApp.DataAccessLayer.Entities;
using System.Threading.Tasks;
using EducationApp.BusinessLogicLayer.Models.User;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using EducationApp.BusinessLogicLayer.Models.Pagination;
using System.Collections.Generic;
using AutoMapper;
using System;

namespace EducationApp.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        UserManager<User> _userManager;
        IUserRepository<User> _userRepository;
        private readonly IMapper _mapper;
        public UserService(UserManager<User> manager, IUserRepository<User> userRepository, IMapper mapper)
        {
            _userManager = manager;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public async Task<string> Create(UserCreationModel userModel)
        {
            var user = _mapper.Map<User>(userModel);
            user.EmailConfirmed = true;
            user.LockoutEnabled = false;
            if(userModel.Password != userModel.PasswordConfirm)
            {
                return "Passwords do not match";
            }
            var isCreated = await _userManager.CreateAsync(user, userModel.Password);
            if (!isCreated.Succeeded)
            {
                return "User added failed";
            }
            var isRoleAdded = await _userManager.AddToRoleAsync(user, "user");
            if (!isRoleAdded.Succeeded)
            {
                return "Adding role failed";
            }   
            return "User: " + user.FirstName + user.LastName + " ,added";
        }
        public async Task<UserForEditModel> GetUser(string userName)
        {
            var responseModel = new UserForEditModel();
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                responseModel.ExecutionResultMessage = "User not found";
                return responseModel;
            }
            responseModel = _mapper.Map<UserForEditModel>(user);
            return responseModel;
        }
        public async Task<UserForEditByAdminModel> GetUserForAdmin(string userID)
        {
            var responseModel = new UserForEditByAdminModel();
            var user = await _userManager.FindByIdAsync(userID);
            if (user == null)
            {
                responseModel.ResultMessage = "User not found";
                return responseModel;
            }
            responseModel = _mapper.Map<UserForEditByAdminModel>(user);
            return responseModel;
        }
        public async Task<UserForEditModel> Edit(UserForEditModel model, string userName)
        {
            var responseModel = new UserForEditModel();
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                responseModel.ExecutionResultMessage = "User not found";
                return responseModel;
            }
            responseModel.ExecutionResultMessage = "Your ";
            var isFirstNameChanged = false;
            if (!string.IsNullOrEmpty(model.FirstName) && (user.FirstName != model.FirstName))
            {
                user.FirstName = model.FirstName;
                isFirstNameChanged = true;
                responseModel.ExecutionResultMessage += "first name, ";
            }
            var isLastNameChanged = false;
            if (!string.IsNullOrEmpty(model.LastName) && (user.LastName != model.LastName))
            {
                user.LastName = model.LastName;
                isLastNameChanged = true;
                responseModel.ExecutionResultMessage += "last name, ";
            }
            var isEmailChanged = false;
            if (!string.IsNullOrEmpty(model.Email) && (user.Email != model.Email))
            {
                var token = await _userManager.GenerateChangeEmailTokenAsync(user, model.Email);
                var identityResult = await _userManager.ChangeEmailAsync(user, model.Email, token);
                isEmailChanged = identityResult.Succeeded;
                responseModel.ExecutionResultMessage += "email, ";
            }
            var isPasswordChanged = false;
            if((!string.IsNullOrEmpty(model.Password) && (model.ConfirmPassword == model.Password)))
            {
                await _userManager.RemovePasswordAsync(user);
                var identityResult = await _userManager.AddPasswordAsync(user, model.Password);
                isPasswordChanged = identityResult.Succeeded;
                responseModel.ExecutionResultMessage += "password, ";
            }
            if(isFirstNameChanged || isLastNameChanged || isEmailChanged || isPasswordChanged)
            {
                var identityResult = await _userManager.UpdateAsync(user);
                responseModel.ExecutionResultMessage += $"update {identityResult} ";
                responseModel = _mapper.Map(user, responseModel);
                return responseModel;
            }
            responseModel = model;
            responseModel.ExecutionResultMessage = "Your profile not updated";
            return responseModel;
        }
        public async Task<UserForEditByAdminModel> EditByAdmin(UserForEditByAdminModel editModel)
        {
            var user = await _userManager.FindByIdAsync(editModel.Id);
            if (user == null)
            {
                editModel.ResultMessage = "User not found";
                return editModel;
            }
            if ((user.FirstName !=editModel.FirstName)|| (user.LastName != editModel.LastName)|| (user.Email != editModel.Email))
            {
                user.LastName = editModel.LastName;
                user.FirstName = editModel.FirstName;
                user.Email = editModel.Email;
                var identityResult = await _userManager.UpdateAsync(user);
                editModel.ResultMessage = $"Update user {identityResult}";
                editModel.IsEdited = true;
                return editModel;
            }
            editModel.ResultMessage = "User not update";
            editModel.IsEdited = false;
            return editModel;
        }

        public async Task<string> SetLockout(string id, bool lockoutEnabled)
        {
            var user = await _userManager.FindByIdAsync(id);
            if(user == null)
            {
                return "user not found";
            }
            IdentityResult identityResult = new IdentityResult();
            if(!lockoutEnabled)
            {
                user.LockoutEnd = null;
                identityResult = await _userManager.UpdateAsync(user);
            }
            if(lockoutEnabled)
            {
                var lockoutTime = DateTime.Now.AddDays(30);
                identityResult = await _userManager.SetLockoutEndDateAsync(user, lockoutTime);
            }
            if (!identityResult.Succeeded)
            {
                return "SetLockout() Failed";
            }
            return "User: " + user.UserName + " set lockout: "+ lockoutEnabled;
        }
        public async Task<UserViewModel> GetUsers(UserRequestModel model)
        {
            UserViewModel viewModel = new UserViewModel();
            viewModel.UserFieldsModels = new List<UserFieldsModel>();
            if (model.PageSize <= 0)
            {
                model.PageSize = 7;
            }
            var responseModel = await _userRepository.GetUserList(model.Page, model.PageSize, model.UserSorting);
            var userList = responseModel.Item1;
            var totalItems = responseModel.totalItems;
            viewModel.UserFieldsModels = _mapper.Map(userList, viewModel.UserFieldsModels);
            var pageInfo = new PageInfo() { TotalItems = totalItems };
            pageInfo = _mapper.Map(model, pageInfo);
            viewModel.PageInfo = pageInfo;
            return viewModel;
        }
        public async Task<RemoveUserResponseModel> Remove(string id)
        {
            var responseModel = new RemoveUserResponseModel();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                responseModel.ResultMessage = "User not found";
                return responseModel;
            }
            user.IsRemoved = true;
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                responseModel.ResultMessage = "Delete failed";
                return responseModel;
            }
            responseModel.ResultMessage = "User: " + user + " deleted";
            responseModel.IsRemoved = true;
            return responseModel;
        }
    }
}
