﻿using EducationApp.BusinessLogicLayer.Models.Author;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IAuthorService
    {
        Task<string> Create(string Name);
        Task<string> GetAuthor(string authorID);
        Task<EditAuthorModel> Edit(EditAuthorModel model);
        Task<RemoveAuthorResponseModel> Remove(string id);
        Task<AuthorViewModel> GetAuthorsWithPrintEdition(AuthorRequestModel model);
        Task<List<AuthorModel>> GetAuthors();
    }
}
