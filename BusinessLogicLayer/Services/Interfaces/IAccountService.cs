﻿using EducationApp.BusinessLogicLayer.Models.Account;
using EducationApp.BusinessLogicLayer.Models.Account.EmailHelper;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IAccountService
    {
        Task<SignUpResponseModel> SignUp(SignUpModel model);
        Task<ForgotPasswordResponseModel> ForgotPassword(string email);
        Task<ResponseTokenModel> SignIn(SignInModel model);
        string SignOut();
        Task<RefreshTokenModel> Refresh(RefreshTokenModel model);
    }
}
