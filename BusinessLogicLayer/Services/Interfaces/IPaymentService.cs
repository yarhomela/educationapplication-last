﻿using EducationApp.BusinessLogicLayer.Models.Payment;
using EducationApp.BusinessLogicLayer.Models.StripePayment;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<PaymentResponseModel> PayAndSave(PaymentRequestModel model, string userName);
        Task<PaymentResponseModel> Pay(PaymentRequestModel model, string userName);
        Task<string> FullRemove(string paymentId);
    }
}
