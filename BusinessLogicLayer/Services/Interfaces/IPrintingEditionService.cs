﻿using EducationApp.BusinessLogicLayer.Models.PrintingEdition;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IPrintingEditionService
    {
        Task<string> Create(PrintEditionModel model);
        Task<PrintEditionModel> Read(string printId);
        Task<PrintEditionForEditModel> Edit(PrintEditionForEditModel requestModel);
        Task<PrintEditionViewModel> GetPrintEditions(PrintEditionRequestModel model);
        Task<RemovePrintEditionResponseModel> Remove(string id);
        Task<string> FullRemove(string printId);
    }

}
