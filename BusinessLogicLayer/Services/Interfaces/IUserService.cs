﻿using EducationApp.BusinessLogicLayer.Models.User;
using System.Threading.Tasks;


namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserService 
    {
        Task<string> Create(UserCreationModel model);
        Task<UserForEditModel> GetUser(string userName);
        Task<UserForEditByAdminModel> GetUserForAdmin(string userID);
        Task<UserForEditModel> Edit(UserForEditModel model, string userName);
        Task<UserForEditByAdminModel> EditByAdmin(UserForEditByAdminModel model);
        Task<string> SetLockout(string id, bool lockoutEnabled);
        Task<UserViewModel> GetUsers(UserRequestModel model);
        Task<RemoveUserResponseModel> Remove(string id);

    }
}