﻿using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition;
using System.Threading.Tasks;

namespace EducationApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IOrderService
    {
        Task<OrderSaveResponseModel> Save(OrderSaveRequestModel orderSaveModel, string userName);
        Task<OrderListResponseModel> GetOrders(OrderListRequestModel requestModel, string userName, bool isAdmin);
        Task<string> FullRemove(string orderId);
        Task<string> FullRemoveOrderItem(string orderItemId);
    }
}
