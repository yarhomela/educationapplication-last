﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net;
using Newtonsoft.Json;
using EducationApp.BusinessLogicLayer.Common.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.PresentationLayer.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _requestDelegate;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate requestDelegate, ILogger logger)
        {
            _logger = logger;
            _requestDelegate = requestDelegate;
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _requestDelegate(context);
            }
            catch (Exception exc)
            {
                await HandleExceptionAsync(context, exc);
            }
        }
        private async Task<Task> HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            string message = string.Empty;
            if(exception is UnauthorizedAccessException)
            {
                code = HttpStatusCode.Unauthorized;
                message = exception.Message;
            }
            if(exception is ValidationException)
            {
                code = HttpStatusCode.UnprocessableEntity;
                message = exception.Message;
            }
            if (exception is NullReferenceException)
            {
                code = HttpStatusCode.Conflict;
                message = exception.Message;
            }
            if(exception is IndexOutOfRangeException)
            {
                code = HttpStatusCode.NotFound;
                message = exception.Message;
            }
            await _logger.LogToFileAsync(exception);

            var result = JsonConvert.SerializeObject(new { error = message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
