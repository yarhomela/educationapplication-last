﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using EducationApp.BusinessLogicLayer.Models.Account;
using EducationApp.BusinessLogicLayer.Models.Account.EmailHelper;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EducationApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(SignUpModel model)
        {
            if (ModelState.IsValid)
            {
                var responseModel = await _accountService.SignUp(model);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("ForgotPassword/{email}")]
        public async Task<IActionResult> ForgotPassword([Required] [EmailAddress] string email)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _accountService.ForgotPassword(email);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn (SignInModel model)
        {
            if(ModelState.IsValid)
            {
                var tokenModel = await _accountService.SignIn(model);
                return Ok(tokenModel);
            }
            return BadRequest();
        }
        [HttpPost("Refresh")]
        public async Task<IActionResult> Refresh(RefreshTokenModel tokenModel)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _accountService.Refresh(tokenModel);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("SignOut")]
        public IActionResult SignOut()
        {
            var response =  _accountService.SignOut();
            return Ok(response);
        }

    }
}