﻿using System.Threading.Tasks;
using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.BusinessLogicLayer.Models.Order.PrintingEdition;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EducationApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpPost("SaveOrder")]
        [Authorize]
        public async Task<IActionResult> SaveOrder(OrderSaveRequestModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _orderService.Save(model, User.Identity.Name);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpPost("GetUserOrders")]
        [Authorize]
        public async Task<IActionResult> GetUserOrders(OrderListRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var responseModel = await _orderService.GetOrders(model, User.Identity.Name, false);
                return Ok(responseModel);
            }
            return BadRequest();
        }
    }
}