﻿using EducationApp.BusinessLogicLayer.Models.StripePayment;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EducationApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class PaymentController : Controller
    {
        private readonly IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }
        [HttpPost("PayAndSave")]
        [Authorize]
        public async Task<IActionResult> PayAndSave(PaymentRequestModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _paymentService.PayAndSave(model, User.Identity.Name);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpPost("Pay")]
        [Authorize]
        public async Task<IActionResult> Pay(PaymentRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var responseModel = await _paymentService.Pay(model, User.Identity.Name);
                return Ok(responseModel);
            }
            return BadRequest();
        }


    }
}