﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using EducationApp.BusinessLogicLayer.Models.PrintingEdition;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EducationApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class PrintingEditionController : ControllerBase
    {
        private readonly IPrintingEditionService _printingEditionService;
        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }
      
        [HttpPost("GetPrintEditions")]
        public async Task<IActionResult> GetPrintEditions(PrintEditionRequestModel model)
        {
            if (ModelState.IsValid)
            {
                var responseModel = await _printingEditionService.GetPrintEditions(model);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("ReadPrintEdition/{printId}")]
        public async Task<IActionResult> ReadPrintEdition( [Required] string printId)
        {
            if (ModelState.IsValid)
            {
                var printModel = await _printingEditionService.Read(printId);
                return Ok(printModel);
            }
            return BadRequest();
        }
    }
}