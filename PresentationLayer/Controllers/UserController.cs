﻿using EducationApp.BusinessLogicLayer.Models.User;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EducationApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet("GetUser")]
        public async Task<IActionResult> GetUser()
        {
            var responseModel = await _userService.GetUser(User.Identity.Name);
            return Ok(responseModel);
        }
        [HttpPost("EditProfile")]
        public async Task<IActionResult> EditProfile(UserForEditModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _userService.Edit(model, User.Identity.Name);
                return Ok(responseModel);
            }
            return BadRequest();
        }
    }
}