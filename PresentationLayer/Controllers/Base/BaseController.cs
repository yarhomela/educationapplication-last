﻿using Microsoft.AspNetCore.Mvc;

namespace EducationApp.PresentationLayer.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
    }
}