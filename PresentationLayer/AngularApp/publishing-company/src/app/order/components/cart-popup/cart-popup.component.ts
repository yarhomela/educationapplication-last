import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { PaymentService } from '../../shared/services/payment.service';
import { PaymentRequestModel } from '../../shared/models/payment-request.model';
import { OrderService } from 'src/app/order/shared/services/order.service';
import { SaveOrderRequestModel } from 'src/app/order/shared/models/save-order-request.model';
import { OrderItemViewModel } from 'src/app/order/shared/models/order-item-view.model';
import { StripeProvider } from '../../shared/services/stripe.provider';
import { PrintEditionModel } from 'src/app/printing-edition/shared/models/print-edition.model';
import { PaymentResponseModel } from '../../shared/models/payment-response.model';
import { OrderSaveResponseModel } from '../../shared/models/save-order-response.model';

@Component({
  selector: 'cart-popup',
  templateUrl: './cart-popup.component.html',
  styleUrls: ['./cart-popup.component.scss']
})
export class CartPopupComponent implements OnInit {
  orderItemViewModel: OrderItemViewModel;
  list: PrintEditionModel[];
  total = 0;
  orderItemModels: OrderItemViewModel[] = new Array();
  currency: number;
  paymentRequestModel: PaymentRequestModel;
  saveOrderRequestModel: SaveOrderRequestModel;
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CartPopupComponent>,
    private stripeProvider: StripeProvider,
    private orderService: OrderService,
    private paymentService: PaymentService, ) { }
  ngOnInit(): void {
    this.orderItemViewModel = {
      quantity: 0,
      printEditionId: '',
      typePrintEdit: 0
    };
    this.paymentRequestModel = {
      orderId: '',
      stripeToken: '',
      stripeEmail: '',
      amount: 0,
      currency: 1
    };
    this.saveOrderRequestModel = {
      amount: 0,
      currency: 1
    };
    this.currency = +localStorage.getItem('currency');
    this.getLocalItems();
  }
  getLocalItems() {
    this.list = JSON.parse(localStorage.getItem('order-items'));
    if (this.list == null) {
      return;
    }
    this.list.forEach(element => {
      const amount = element.price * element.quantity;
      this.total = this.total + amount;
    });
  }
  close() {
    this.dialogRef.close();
  }
  remove(printEdition: PrintEditionModel) {
    const index = this.list.indexOf(printEdition);
    this.list.splice(index, 1);
    const parsedCartItemsList = JSON.stringify(this.list);
    localStorage.setItem('order-items', parsedCartItemsList);
    const cartCount = +localStorage.getItem('cart-count') - 1;
    localStorage.setItem('cart-count', cartCount.toString());
    this.getLocalItems();
  }
  async payOrder() {
    const paymentModel = this.paymentRequestModel;
    paymentModel.amount = this.total * 100;
    paymentModel.currency = this.currency;
    this.list.forEach(element => {
      const orderItem = this.orderItemViewModel;
      orderItem.printEditionId = element.id;
      orderItem.quantity = element.quantity;
      orderItem.typePrintEdit = element.typePrintEdit;
      this.orderItemModels.push(orderItem);
    });
    paymentModel.orderItemModels = this.orderItemModels;
    const token = await this.stripeProvider.openStripe(paymentModel.currency, paymentModel.amount);
    if (token) {
      paymentModel.stripeEmail = token.email;
      paymentModel.stripeToken = token.id;
      this.paymentService.payAndSave(paymentModel).subscribe((paymentResponse: PaymentResponseModel) => {
        console.log(paymentResponse.executionResultMessage);
        if (paymentResponse.isSuccess) {
          localStorage.setItem('cart-count', '0');
          localStorage.removeItem('order-items');
          this.getLocalItems();
        }
      });
    }
  }
  saveOrder() {
    const orderSaveModel = this.saveOrderRequestModel;
    orderSaveModel.amount = this.total * 100;
    orderSaveModel.currency = this.currency;
    this.list.forEach(element => {
      const orderItem = this.orderItemViewModel;
      orderItem.printEditionId = element.id;
      orderItem.quantity = element.quantity;
      orderItem.typePrintEdit = element.typePrintEdit;
      this.orderItemModels.push(orderItem);
    });
    orderSaveModel.orderItemModels = this.orderItemModels;
    this.orderService.saveOrder(orderSaveModel).subscribe((responseModel: OrderSaveResponseModel) => {
      console.log(responseModel.executionResultMessage);
      if (responseModel.isSaved) {
        localStorage.setItem('cart-count', '0');
        localStorage.removeItem('order-items');
        this.getLocalItems();
      }
    });
  }
}
