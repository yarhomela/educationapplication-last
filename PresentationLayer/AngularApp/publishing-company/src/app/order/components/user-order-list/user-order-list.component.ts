import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../shared/services/order.service';
import { OrderListRequestModel } from '../../shared/models/order-list-request.model';
import { OrderListResponseModel } from '../../shared/models/order-list-response.model';
import { OrderFieldsModel } from '../../shared/models/order-fields.model';
import { PageInfoModel } from 'src/app/admin/shared/models/page-info.model';
import { PaymentRequestModel } from '../../shared/models/payment-request.model';
import { StripeProvider } from '../../shared/services/stripe.provider';
import { PaymentService } from '../../shared/services/payment.service';
import { PaymentResponseModel } from '../../shared/models/payment-response.model';

@Component({
  selector: 'user-order',
  templateUrl: './user-order-list.component.html',
  styleUrls: ['./user-order-list.component.scss']
})
export class UserOrdersComponent implements OnInit {
  paymentRequestModel: PaymentRequestModel;
  orderListRequestModel: OrderListRequestModel;
  listModel: OrderFieldsModel[] = new Array();
  pageInfo: PageInfoModel;
  pages: number[];
  constructor(private orderService: OrderService, private stripeProvider: StripeProvider, private paymentService: PaymentService) { }
  ngOnInit() {
    this.orderListRequestModel = {
      page: 1,
      pageSize: 6,
      status: 0,
      orderSorting: 0
    };
    this.paymentRequestModel = {
      orderId: '',
      stripeToken: '',
      stripeEmail: '',
      amount: 0,
      currency: 1
    }
    this.orderListRequestModel.page = 1;
    this.getUserOrders(this.orderListRequestModel);
  }
  getUserOrders(orderListRequestModel: OrderListRequestModel) {
    this.orderService.getUserOrders(orderListRequestModel).subscribe((responseModel: OrderListResponseModel) => {
      this.listModel = responseModel.orderFieldsList;
      this.pageInfo = responseModel.pageInfo;
      this.pages = new Array();
      for (let i = 1; i <= responseModel.pageInfo.totalPages; i++) {
        this.pages.push(i);
      }
    });
  }
  async payOrder(order: OrderFieldsModel) {
    const paymentModel = this.paymentRequestModel;
    paymentModel.orderId = order.id;
    paymentModel.amount = order.amount * 100;
    paymentModel.currency = +localStorage.getItem('currency');
    const token = await this.stripeProvider.openStripe(paymentModel.currency, paymentModel.amount);
    if (token) {
      paymentModel.stripeEmail = token.email;
      paymentModel.stripeToken = token.id;
      this.paymentService.pay(paymentModel).subscribe((responseModel: PaymentResponseModel) => {
        console.log(responseModel.executionResultMessage);
        if (responseModel.isSuccess) {
          this.getUserOrders(this.orderListRequestModel);
        }
      });
    }
  }
  pagination(pageNumber: number) {
    this.orderListRequestModel.page = pageNumber;
    this.getUserOrders(this.orderListRequestModel);
  }
}
