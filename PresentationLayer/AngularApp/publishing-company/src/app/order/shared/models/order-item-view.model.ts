import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';

export interface OrderItemViewModel {
    quantity: number;
    printEditionId: string;
    typePrintEdit: TypePrintEditEnum;
}
