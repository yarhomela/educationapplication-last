export interface PaymentResponseModel {
    executionResultMessage: string;
    isSuccess: boolean;
}
