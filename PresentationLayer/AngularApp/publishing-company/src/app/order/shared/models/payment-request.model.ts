import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';
import { OrderItemViewModel } from './order-item-view.model';

export interface PaymentRequestModel {
    orderItemModels?: OrderItemViewModel[];
    description?: string;
    orderId: string;
    stripeToken: string;
    stripeEmail: string;
    amount: number;
    currency: CurrencyEnum;
}
