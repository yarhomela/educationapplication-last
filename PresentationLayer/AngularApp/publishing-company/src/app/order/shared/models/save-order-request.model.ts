import { OrderItemViewModel } from './order-item-view.model';
import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';

export interface SaveOrderRequestModel {
    orderItemModels?: OrderItemViewModel[];
    description?: string;
    amount: number;
    currency: CurrencyEnum;
}
