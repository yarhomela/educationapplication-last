import { PageInfoModel } from 'src/app/admin/shared/models/page-info.model';
import { OrderFieldsModel } from './order-fields.model';

export interface OrderListResponseModel {
    orderFieldsList: OrderFieldsModel[];
    pageInfo: PageInfoModel;
}
