export interface OrderSaveResponseModel {
    executionResultMessage: string;
    isSaved: boolean;
}
