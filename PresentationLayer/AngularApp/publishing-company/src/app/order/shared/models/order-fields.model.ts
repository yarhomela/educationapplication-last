import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';
import { OrderItemViewModel } from './order-item-view.model';
import { StatusEnum } from 'src/app/shared/enums/status.enum';

export interface OrderFieldsModel {
    id: string;
    orderTime: Date;
    paymentTime: Date;
    orderItemsList: OrderItemViewModel[];
    currency: CurrencyEnum;
    amount: number;
    status: StatusEnum;
    paymentId: string;
    userName: string;
    email: string;
}
