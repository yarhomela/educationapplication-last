import { PrintEditionModel } from 'src/app/printing-edition/shared/models/print-edition.model';

export interface CartDataModel {
    items: PrintEditionModel[];
}
