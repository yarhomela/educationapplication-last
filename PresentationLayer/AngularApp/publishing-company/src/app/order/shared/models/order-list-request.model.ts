import { StatusEnum } from '../../../shared/enums/status.enum';
import { OrderSortingEnum } from 'src/app/shared/enums/orderSorting.enum';

export interface OrderListRequestModel {
    page: number;
    pageSize: number;
    status: StatusEnum;
    orderSorting: OrderSortingEnum;
}
