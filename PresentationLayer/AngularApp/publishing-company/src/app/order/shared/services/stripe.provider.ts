import { Injectable } from '@angular/core';
import { StripeCheckoutHandler, StripeCheckoutLoader } from 'ng-stripe-checkout';
import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StripeProvider {
  constructor(private stripeCheckoutLoader: StripeCheckoutLoader) { }
  private stripeCheckoutHandler: StripeCheckoutHandler;
  public getToken(): string {
    return localStorage.getItem('access-token');
  }


  async openStripe(currency: CurrencyEnum, amount: number) {
    this.stripeCheckoutHandler = await this.stripeCheckoutLoader.createHandler({
      key: environment.key,
    });
    const checkedCurrency = CurrencyEnum[currency];
    return this.stripeCheckoutHandler.open({
      locale: 'en',
      amount: (amount),
      currency: checkedCurrency
    });
  }
}
