import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentRequestModel } from '../models/payment-request.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  constructor(private http: HttpClient) { }
  baseurl = environment.baseURL + 'Payment/';
  result: string;
  public getToken(): string {
    return localStorage.getItem('access-token');
  }
  payAndSave(paymentRequestModel: PaymentRequestModel): Observable<any> {
    const url = this.baseurl + 'PayAndSave';
    return this.http.post(url, paymentRequestModel);
  }
  pay(paymentRequestModel: PaymentRequestModel): Observable<any> {
    const url = this.baseurl + 'Pay';
    return this.http.post(url, paymentRequestModel);
  }
}
