import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SaveOrderRequestModel } from '../models/save-order-request.model';
import { OrderListRequestModel } from '../models/order-list-request.model';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    constructor(private http: HttpClient) {
    }
    baseurl = environment.baseURL + 'Order/';
    saveOrder(saveOrderRequestModel: SaveOrderRequestModel): Observable<any> {
        const url = this.baseurl + 'SaveOrder';
        return this.http.post(url, saveOrderRequestModel);
    }
    getUserOrders(orderListRequestModel: OrderListRequestModel) {
        const url = this.baseurl + 'GetUserOrders/';
        return this.http.post(url, orderListRequestModel);
    }
}
