import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CartPopupComponent } from './components/cart-popup/cart-popup.component';

import { UserOrdersComponent } from './components/user-order-list/user-order-list.component';
import { StripeCheckoutModule } from 'ng-stripe-checkout';
import { PaymentService } from './shared/services/payment.service';
import { OrderService } from './shared/services/order.service';
import { StripeProvider } from './shared/services/stripe.provider';

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule, MatDialogModule, CartPopupComponent, StripeCheckoutModule],
    exports: [UserOrdersComponent, CartPopupComponent],
    declarations: [UserOrdersComponent, CartPopupComponent],
    providers: [HttpClientModule, MatDialogRef, PaymentService, OrderService, StripeProvider],
})
export class OrderModule {

}
