import { Component, OnInit } from '@angular/core';
import { OrderListRequestModel } from '../../../order/shared/models/order-list-request.model';
import { OrderListResponseModel } from '../../../order/shared/models/order-list-response.model';
import { OrderFieldsModel } from '../../../order/shared/models/order-fields.model';
import { AdminService } from '../../shared/service/admin.service';

@Component({
    selector: 'order-managment',
    templateUrl: './order-managment.component.html',
    styleUrls: ['./order-managment.component.scss']
})
export class OrderManagementComponent implements OnInit {
    requestModel: OrderListRequestModel;
    orderList: OrderFieldsModel[] = new Array();
    pages: number[];
    constructor(
        private adminService: AdminService) { }
    ngOnInit() {
        this.requestModel = {
            page: 1,
            pageSize: 6,
            status: 0,
            orderSorting: 0
        };
        this.getAdminOrders(this.requestModel);
    }
    getAdminOrders(requestModel: OrderListRequestModel) {
        this.adminService.getAdminOrders(requestModel).subscribe((responseModel: OrderListResponseModel) => {
            this.orderList = responseModel.orderFieldsList;
            this.pages = new Array();
            for (let i = 1; i <= responseModel.pageInfo.totalPages; i++) {
                this.pages.push(i);
            }
        });
    }
    pagination(pageNumber: number) {
        this.requestModel.page = pageNumber;
        this.getAdminOrders(this.requestModel);
    }
}
