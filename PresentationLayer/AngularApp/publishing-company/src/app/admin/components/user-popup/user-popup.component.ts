import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AdminService } from '../../shared/service/admin.service';
import { UserForEditByAdminModel } from 'src/app/user/shared/models/user-for-edit-by-admin.model';
import { UserCreationModel } from 'src/app/user/shared/models/user-creation.model';

@Component({
  selector: 'user',
  templateUrl: './user-popup.component.html',
  styleUrls: ['./user-popup.component.scss']
})
export class UserPopupComponent implements OnInit {
  userCreationModel: UserCreationModel;
  userModel: UserForEditByAdminModel;
  isCreated: boolean;
  titleText: string;

  constructor(
    private adminService: AdminService,
    public dialogRef: MatDialogRef<UserPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public userId: string) { }

  ngOnInit() {
    this.userCreationModel = {
      email : 'E-mail',
      firstName : 'First Name',
      lastName : 'Last Name',
      password : '',
      passwordConfirm : ''
     };
    this.userModel = {
      id: '',
      firstName: '',
      lastName: '',
      email: '',
      isEdited : false,
      resultMessage: ''
    };
    if (this.userId != null) {
        this.getUser(this.userId);
        this.titleText = 'Edit user';
        return;
    }
    this.isCreated = true;
    this.titleText = 'Create a new user';
    }

  close(responseMessage: string) {
    this.dialogRef.close(responseMessage);
  }
  getUser(userID: string) {
    this.adminService.getUser(userID).subscribe((responseModel: UserForEditByAdminModel) => {
      this.userModel = responseModel;
      this.userModel.id = userID;
    });
  }
  editUser(userModel: UserForEditByAdminModel) {
    this.adminService.editUser(userModel).subscribe((responseModel: UserForEditByAdminModel) => {
      this.userModel = responseModel;
      if (responseModel.isEdited) {
        console.log(responseModel.resultMessage);
        this.close(responseModel.resultMessage);
      }
    });
  }
  addUser(newUserModel: UserCreationModel) {
    newUserModel.firstName = this.userModel.firstName;
    newUserModel.lastName = this.userModel.lastName;
    newUserModel.email = this.userModel.email;
    this.adminService.addUser(newUserModel).subscribe(response => {
      console.log(response);
      this.close(response);
    });
  }
}
