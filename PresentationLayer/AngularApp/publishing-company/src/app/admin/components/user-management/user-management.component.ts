import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserPopupComponent } from '../user-popup/user-popup.component';
import { UserFieldsModel } from '../../../user/shared/models/user-fields.model';
import { UserViewModel } from '../../../user/shared/models/user-view.model';
import { UserRequestModel } from '../../../user/shared/models/user-request.model';
import { AdminService } from '../../shared/service/admin.service';
import { MessagePopupComponent } from 'src/app/message-popup/message-popup.component';
import { RemoveUserResponseModel } from 'src/app/user/shared/models/remove-user-response.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PageInfoModel } from '../../shared/models/page-info.model';


@Component({
    selector: 'users-managment',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
    user: string;
    pages: number[];
    pageInfo: PageInfoModel;
    userList: UserFieldsModel[] = new Array();
    requestModel: UserRequestModel;
    constructor(public dialog: MatDialog, private adminService: AdminService, private snackBar: MatSnackBar) { }
    ngOnInit() {
        this.requestModel = {
            page: 1,
            pageSize: 6,
            userSorting: 0
        };
        this.getUsers(this.requestModel);
    }
    openUser(userId: string) {
        const openDialog = this.dialog.open(UserPopupComponent, {
            data: userId
        });
        openDialog.afterClosed().subscribe(response => {
            if (response != null) {
                this.getUsers(this.requestModel);
                this.snackBar.open(response, 'Ok', {
                    duration: 2000,
                });
            }
        })
    }
    getUsers(requestModel: UserRequestModel) {
        this.adminService.getUsers(requestModel).subscribe((responceModel: UserViewModel) => {
            this.userList = responceModel.userFieldsModels;
            this.pageInfo = responceModel.pageInfo;
            this.pages = new Array();
            for (let i = 1; i <= responceModel.pageInfo.totalPages; i++) {
                this.pages.push(i);
            }
        })
    }
    setLockout(userId: string, lockoutEnabled: boolean) {
        this.adminService.setLockoutUser(userId, lockoutEnabled).subscribe(result => {
            console.log(result);
        })
    }
    removeUser(userId: string) {
        const textMessage = 'You are shure remove this User?';
        const message = this.dialog.open(MessagePopupComponent, {
            data: textMessage
        });
        message.afterClosed().subscribe(result => {
            if (result) {
                this.adminService.removeUser(userId).subscribe((responseModel: RemoveUserResponseModel) => {
                    console.log(responseModel.resultMessage);
                    this.updatePageAfterRemoved(responseModel.isRemoved);
                    this.snackBar.open(responseModel.resultMessage, 'Ok', {
                        duration: 2000,
                    });
                });
            }
        });
    }
    pagination(pageNumber: number) {
        this.requestModel.page = pageNumber;
        this.getUsers(this.requestModel);
    }
    private updatePageAfterRemoved(isRemoved: boolean) {
        if (!isRemoved){
            return;
        }
        const currentTotalItems = this.pageInfo.totalItems - 1;
        const currentTotalPages = Math.ceil(currentTotalItems / this.pageInfo.pageSize);
        const isEmtyPage = (currentTotalPages < this.requestModel.page);
        if (isEmtyPage) {
            this.requestModel.page += -1;
        }
        this.getUsers(this.requestModel);
    }
}
