import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AdminService } from '../../shared/service/admin.service';
import { EditAuthorModel } from '../../shared/models/edit-author.model';

@Component({
  selector: 'author-popup',
  templateUrl: './author-popup.component.html',
  styleUrls: ['./author-popup.component.scss']
})
export class AuthorPopupComponent  implements OnInit{
  titleText: string;
  isNewAuthor: boolean;
  authorModel: EditAuthorModel;
  constructor(
    private adminService: AdminService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<AuthorPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public authorId: string) { }
  ngOnInit() {
    this.authorModel = {
      id: '',
      name: '',
      editResultMessage: '',
      isEdited: false
    };
    if (this.getPurposeAddOrEdit()) {
      this.getAuthor(this.authorId);
    }
  }
  close(responseMessage: string) {
    this.dialogRef.close(responseMessage);
  }
  getAuthor(authorID: string) {
    this.adminService.getAuthor(authorID).subscribe((authorName: string) => {
      this.authorModel.name = authorName;
      this.authorModel.id = this.authorId;
    })
  }
  editAuthor(authorModel: EditAuthorModel) {
    this.adminService.editAuthor(authorModel).subscribe((responseModel: EditAuthorModel) => {
      this.authorModel = responseModel;
      if (responseModel.isEdited) {
        this.close(responseModel.editResultMessage);
      }
    })
  }
  addAuthor(authorName: string) {
    this.adminService.addAuthor(authorName).subscribe(response => {
      this.close(response);
    })
  }

  getPurposeAddOrEdit() {
    if (this.authorId != null) {
      this.titleText = 'Edit author';
      this.isNewAuthor = false;
      return true;
    }
    this.titleText = 'Add new author';
    this.isNewAuthor = true;
  }
}
