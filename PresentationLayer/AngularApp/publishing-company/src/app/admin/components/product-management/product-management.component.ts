import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PrintEditionPopupComponent } from '../print-add-popup/print-edition-popup.component';
import { MessagePopupComponent } from 'src/app/message-popup/message-popup.component';
import { AdminService } from '../../shared/service/admin.service';
import { PrintEditionFieldsModel } from 'src/app/printing-edition/shared/models/print-edition-fields.model';
import { PrintEditionRequestModel } from 'src/app/printing-edition/shared/models/print-edition-receive.model';
import { PrintEditionsModel } from 'src/app/printing-edition/shared/models/print-edition-view.model';
import { EnumToListHelper } from 'src/app/shared/helpers/enum-to-list.service';
import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';
import { RemovePrintEditionResponseModel } from 'src/app/printing-edition/shared/models/remove-print-edition-response.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PageInfoModel } from '../../shared/models/page-info.model';

@Component({
    selector: 'product-management',
    templateUrl: './product-management.component.html',
    styleUrls: ['./product-management.component.scss']

})
export class ProductManagementComponent implements OnInit {
    printEditionList: PrintEditionFieldsModel[];
    printEditionRequest: PrintEditionRequestModel;
    pages: number[];
    pageInfo: PageInfoModel;
    sortingOptions: Array<number>;
    constructor(
        public dialog: MatDialog, private adminService: AdminService,
        private enumToListHelper: EnumToListHelper, private snackBar: MatSnackBar) { }
    ngOnInit() {
        this.printEditionRequest = {
            page: 0,
            pageSize: 6,
            printSorting: 0,
            typePrintEdit: 0,
            searchByTitle: '',
            minPrice: 0,
            maxPrice: 0
        };
        this.printEditionRequest.page = 1;
        this.getPrintEditions(this.printEditionRequest);
        this.sortingOptions = this.enumToListHelper.generateListNumber(TypePrintEditEnum);
    }

    openPrint(printId: string) {
        const openDialog = this.dialog.open(PrintEditionPopupComponent, {
            data: printId
        });
        openDialog.afterClosed().subscribe(responseMessage => {
            if (responseMessage != null) {
                this.getPrintEditions(this.printEditionRequest);
                this.snackBar.open(responseMessage, 'Ok', {
                    duration: 2000,
                });
            }
        });
    }
    getPrintEditions(printEditionRequest: PrintEditionRequestModel) {
        this.adminService.getPrintEditions(printEditionRequest).subscribe((responseModel: PrintEditionsModel) => {
            this.printEditionList = responseModel.printingEditionFieldsModels;
            this.pageInfo = responseModel.pageInfo;

            this.pages = new Array();
            for (let i = 1; i <= responseModel.pageInfo.totalPages; i++) {
                this.pages.push(i);
            }
        });
    }
    remove(printId: string) {
        const textMessage = 'You are shure remove this item?';
        const message = this.dialog.open(MessagePopupComponent, {
            data: textMessage
        });
        message.afterClosed().subscribe(result => {
            if (result) {
                this.adminService.removePrintEdition(printId).subscribe((responseModel: RemovePrintEditionResponseModel) => {
                    this.updatePageAfterRemoved(responseModel.isRemoved);
                    console.log(responseModel.resultMessage);
                    this.snackBar.open(responseModel.resultMessage, 'Ok', {
                        duration: 2000,
                    });
                });
            }
        });
    }
    pagination(pageNumber: number) {
        this.printEditionRequest.page = pageNumber;
        this.getPrintEditions(this.printEditionRequest);
    };
    private updatePageAfterRemoved(isRemoved: boolean) {
        if (!isRemoved){
            return;
        }
        const currentTotalItems = this.pageInfo.totalItems - 1;
        const currentTotalPages = Math.ceil(currentTotalItems / this.pageInfo.pageSize);
        const isEmtyPage = (currentTotalPages < this.printEditionRequest.page);
        if (isEmtyPage) {
            this.printEditionRequest.page += -1;
        }
        this.getPrintEditions(this.printEditionRequest);
    }
}
