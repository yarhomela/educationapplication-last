import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthorFieldsModel } from '../../shared/models/author-fields.model';
import { AdminService } from '../../shared/service/admin.service';
import { AuthorViewModel } from '../../shared/models/author-view.model';
import { MessagePopupComponent } from 'src/app/message-popup/message-popup.component';
import { PageInfoModel } from '../../shared/models/page-info.model';
import { AuthorPopupComponent } from '../author-popup/author-popup.component';
import { AuthorRequestModel } from '../../shared/models/author-request.model';
import { RemoveAuthorResponseModel } from '../../shared/models/remove-author-response.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'author-management',
    templateUrl: './author-management.component.html',
    styleUrls: ['./author-management.component.scss']
})
export class AuthorManagementComponent implements OnInit {
    requestModel: AuthorRequestModel;
    authorsList: AuthorFieldsModel[] = new Array();
    pageInfo: PageInfoModel;
    pages: number[];
    constructor(public dialog: MatDialog, private adminService: AdminService, private snackBar: MatSnackBar) { }
    ngOnInit() {
        this.requestModel = {
            page: 1,
            pageSize: 6,
            authorSorting: 0
        };
        this.getAuthorsWithPrintEdition(this.requestModel);
    }

    openAuthor(authorId?: string) {
        if (authorId == null) {
            authorId = null;
        }
        const openDialog = this.dialog.open(AuthorPopupComponent, {
            data: authorId
        });
        openDialog.afterClosed().subscribe(response => {
            if (response != null) {
                console.log(response);
                this.getAuthorsWithPrintEdition(this.requestModel);
                this.snackBar.open(response, 'Ok', {
                    duration: 2000,
                });
            }
        });
    }

    getAuthorsWithPrintEdition(requestModel: AuthorRequestModel) {
        this.adminService.getAuthorsWithPrintEdition(requestModel).subscribe((responseModel: AuthorViewModel) => {
            this.pageInfo = responseModel.pageInfo;
            this.authorsList = responseModel.authorFields;
            this.pages = new Array();
            for (let i = 1; i <= responseModel.pageInfo.totalPages; i++) {
                this.pages.push(i);
            }
        });
    }
    removeAuthor(author) {
        const textMessage = 'You are sure want to delete this author?';
        const message = this.dialog.open(MessagePopupComponent, {
            data: textMessage
        });
        message.afterClosed().subscribe(result => {
            if (result) {
                this.adminService.removeAuthor(author.id).subscribe((responseModel: RemoveAuthorResponseModel) => {
                    this.updatePageAfterRemoved(responseModel.isRemoved);
                    console.log(responseModel.resultMessage);
                    this.snackBar.open(responseModel.resultMessage, 'Ok', {
                        duration: 2000,
                    });
                });
            }
        });
    }
    pagination(pageNumber) {
        this.requestModel.page = pageNumber;
        this.getAuthorsWithPrintEdition(this.requestModel);
    }
    private updatePageAfterRemoved(isRemoved: boolean) {
        if (!isRemoved) {
            return;
        }
        const currentTotalItems = this.pageInfo.totalItems - 1;
        const currentTotalPages = Math.ceil(currentTotalItems / this.pageInfo.pageSize);
        const isEmtyPage = (currentTotalPages < this.requestModel.page);
        if (isEmtyPage) {
            this.requestModel.page += -1;
        }
        this.getAuthorsWithPrintEdition(this.requestModel);
    }
}
