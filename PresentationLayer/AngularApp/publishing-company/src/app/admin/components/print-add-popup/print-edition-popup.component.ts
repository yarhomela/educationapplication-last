import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AdminService } from '../../shared/service/admin.service';
import { PrintEditionForEditModel } from 'src/app/printing-edition/shared/models/print-edition-for-edit.model';
import { AuthorModel } from '../../shared/models/author.model';
import { EnumToListHelper } from 'src/app/shared/helpers/enum-to-list.service';
import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';
import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';

@Component({
  selector: 'print-edition-popup',
  templateUrl: './print-edition-popup.component.html',
  styleUrls: ['./print-edition-popup.component.scss']
})
export class PrintEditionPopupComponent implements OnInit {
  printEdition: PrintEditionForEditModel;
  titleText: string;
  isNewPrint: boolean;
  authors: AuthorModel[] = new Array();
  categorySelection: Array<number>;
  currencySelection: Array<number>;
  constructor(
    public dialogRef: MatDialogRef<PrintEditionPopupComponent>, private adminService: AdminService,
    public dialog: MatDialog, private enumToListHelper: EnumToListHelper,
    @Inject(MAT_DIALOG_DATA) public printId: string) { }
  ngOnInit() {
    this.printEdition = {
      id: '',
      title: '',
      description: '',
      price: 0,
      currency: 0,
      typePrintEdit: 0,
      authorId: ''
    };
    this.getAuthors();
    this.categorySelection = this.enumToListHelper.generateListNumber(TypePrintEditEnum);
    this.currencySelection = this.enumToListHelper.generateListNumber(CurrencyEnum);
    if (this.getPurposeAddOrEdit()) {
       this.getPrintEdition(this.printId);
    }
  }
  close(responseMessage) {
    this.dialogRef.close(responseMessage);
  }
  getAuthors() {
    this.adminService.getAuthors().subscribe(responseAuthors => {
      this.authors = responseAuthors;
    });
  }
  addPrintEdition(printEdition: PrintEditionForEditModel) {
    this.adminService.addPrintEdition(printEdition).subscribe(response => {
      console.log(response);
      this.close(response);
    });
  }
  getPrintEdition(printId: string) {
    this.adminService.getPrintEdition(printId).subscribe((responseModel: PrintEditionForEditModel) => {
      this.printEdition = responseModel;
      this.printEdition.id = this.printId;
    });

  }
  editPrintEdition(printEdition: PrintEditionForEditModel) {
    this.adminService.editPrintEdition(this.printEdition).subscribe((responseModel: PrintEditionForEditModel) => {
      this.printEdition = responseModel;
      this.close(responseModel.resultMessage);
    });
  }
  getPurposeAddOrEdit() {
    if (this.printId != null) {
      this.titleText = 'Edit print edition';
      this.isNewPrint = false;
      return true;
    }
    this.titleText = 'Add new print edition';
    this.isNewPrint = true;
  }
}
