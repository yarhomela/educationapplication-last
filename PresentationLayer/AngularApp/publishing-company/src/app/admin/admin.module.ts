import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule} from '@angular/router';
import { AdminService } from './shared/service/admin.service';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { AuthorManagementComponent } from './components/author-management/author-management.component';
import { UserPopupComponent } from './components/user-popup/user-popup.component';
import { OrderManagementComponent } from './components/order-managment/order-managment.component';
import { PrintEditionPopupComponent } from './components/print-add-popup/print-edition-popup.component';
import { MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { AuthorPopupComponent } from './components/author-popup/author-popup.component';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';


@NgModule({
    imports: [RouterModule,FormsModule,BrowserModule,AuthorPopupComponent,UserPopupComponent,PrintEditionPopupComponent,MatDialogModule,MatSnackBarModule],
    exports: [ UserManagementComponent,AuthorManagementComponent,OrderManagementComponent],
    declarations: [UserManagementComponent,AuthorManagementComponent,OrderManagementComponent ],
    providers:[HttpClientModule,AdminService,MatDialogModule, MatDialogRef,MatSnackBar ]
})
export class AdminModule{
    
}