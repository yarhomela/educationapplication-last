import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorModel } from '../models/author.model';
import { AuthorRequestModel } from '../models/author-request.model';
import { OrderListRequestModel } from 'src/app/order/shared/models/order-list-request.model';
import { PrintEditionForEditModel } from 'src/app/printing-edition/shared/models/print-edition-for-edit.model';
import { PrintEditionRequestModel } from 'src/app/printing-edition/shared/models/print-edition-receive.model';
import { UserCreationModel } from 'src/app/user/shared/models/user-creation.model';
import { UserRequestModel } from 'src/app/user/shared/models/user-request.model';
import { UserForEditByAdminModel } from 'src/app/user/shared/models/user-for-edit-by-admin.model';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminService {
    constructor(private http: HttpClient) {
    }
    baseurl = environment.baseURL + 'Admin/';
    addAuthor(authorName: string): Observable<any> {
        const url = this.baseurl + 'AddAuthor/' + authorName;
        return this.http.get(url);
    }
    getAuthor(authorID: string) {
        const url = this.baseurl + 'GetAuthor/' + authorID;
        return this.http.get(url);
    }
    editAuthor(authorModel: AuthorModel): Observable<any> {
        const url = this.baseurl + 'EditAuthor';
        return this.http.post(url, authorModel);
    }
    removeAuthor(authorId: string): Observable<any> {
        const url = this.baseurl + 'RemoveAuthor/' + authorId;
        return this.http.get(url);
    }
    getAuthorsWithPrintEdition(authorRequestModel: AuthorRequestModel): Observable<any> {
        const url = this.baseurl + 'GetAuthorsWithPrintEdition';
        return this.http.post(url, authorRequestModel);
    }

    getAdminOrders(orderListRequestModel: OrderListRequestModel): Observable<any> {
        const url = this.baseurl + 'GetAdminOrders';
        return this.http.post(url, orderListRequestModel);
    }

    addPrintEdition(printEditionModel: PrintEditionForEditModel): Observable<any> {
        const url = this.baseurl + 'AddPrintEdition';
        return this.http.post(url, printEditionModel);
    }
    editPrintEdition(printEditionForEditModel: PrintEditionForEditModel): Observable<any> {
        const url = this.baseurl + 'EditPrintEdition';
        return this.http.post(url, printEditionForEditModel);
    }
    getPrintEdition(printEditionID: string) {
        const url = this.baseurl + 'GetPrintEdition/' + printEditionID;
        return this.http.get(url);
    }
    getPrintEditions(printEditionRequestModel: PrintEditionRequestModel): Observable<any> {
        const url = this.baseurl + 'GetPrintEditions';
        return this.http.post(url, printEditionRequestModel);
    }
    removePrintEdition(printId: string): Observable<any> {
        const url = this.baseurl + 'RemovePrintEdition/' + printId;
        return this.http.get(url);
    }
    getAuthors(): Observable<any> {
        const url = this.baseurl + 'GetAuthors';
        return this.http.get(url);
    }

    addUser(userCreationModel: UserCreationModel): Observable<any> {
        const url = this.baseurl + 'AddUser';
        return this.http.post(url, userCreationModel);
    }
    removeUser(userId: string): Observable<any> {
        const url = this.baseurl + 'RemoveUser/' + userId;
        return this.http.get(url);
    }
    setLockoutUser(userId: string, lockoutEnabled: boolean): Observable<any> {
        const url = this.baseurl + 'UserSetLockout/' + userId + '/' + lockoutEnabled;
        return this.http.get(url);
    }
    getUsers(userRequestModel: UserRequestModel): Observable<any> {
        const url = this.baseurl + 'GetUsers';
        return this.http.post(url, userRequestModel);
    }
    getUser(userID: string) {
        const url = this.baseurl + 'GetUserForAdmin/' + userID;
        return this.http.get(url);
    }
    editUser(userForEditByAdminModel: UserForEditByAdminModel): Observable<any> {
        const url = this.baseurl + 'EditUser';
        return this.http.post(url, userForEditByAdminModel);
    }
}
