export interface AuthorFieldsModel {
    id: string;
    name: string;
    products: string[];
}
