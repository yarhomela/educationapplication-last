import { PageInfoModel } from 'src/app/admin/shared/models/page-info.model';
import { AuthorFieldsModel } from './author-fields.model';

export interface AuthorViewModel {
    authorFields: AuthorFieldsModel[];
    pageInfo: PageInfoModel;
}
