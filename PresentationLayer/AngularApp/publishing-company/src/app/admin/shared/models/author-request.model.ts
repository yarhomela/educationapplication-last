import { AuthorSortingEnum } from 'src/app/shared/enums/author-sorting.enum';

export interface AuthorRequestModel {
    page: number;
    pageSize: number;
    authorSorting: AuthorSortingEnum;
}
