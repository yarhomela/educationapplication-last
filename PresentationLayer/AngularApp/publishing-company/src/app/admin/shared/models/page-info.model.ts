export interface PageInfoModel {
    pageNumber: number;
    pageSize: number;
    totalItems: number;
    totalPages: number;
}
