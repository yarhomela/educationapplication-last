export interface EditAuthorModel {
    id: string;
    name: string;
    editResultMessage: string;
    isEdited: boolean;
}
