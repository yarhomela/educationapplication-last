export interface RemoveAuthorResponseModel {
    isRemoved: boolean;
    resultMessage: string;
}
