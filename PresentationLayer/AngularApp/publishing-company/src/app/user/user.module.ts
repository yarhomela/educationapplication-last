import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { UserService } from './shared/service/user.service';

import { UserProfileComponent } from './components/user-profile.component/user-profile.component';

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule],
    exports: [UserProfileComponent],
    declarations: [UserProfileComponent],
    providers: [HttpClientModule, UserService],
})
export class UserModule {

}
