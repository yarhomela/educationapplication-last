import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/service/user.service';
import { UserForEditModel } from '../../shared/models/user-for-edit.model';

@Component({
    selector: 'user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})

export class UserProfileComponent implements OnInit {
    userModel: UserForEditModel;
    isHidden = true;
    constructor(private userService: UserService) { }
    ngOnInit() {
        this.getUser();
        this.userModel = {
            firstName: 'First Name',
            lastName: 'Last Name',
            email: 'E-mail',
            password: '',
            confirmPassword: '',
            executionResultMessage: ''
        };
    }
    getUser() {
        this.userService.getUser().subscribe((responseModel: UserForEditModel) => {
            this.userModel = responseModel;
        });
    }
    editProfile(userModel: UserForEditModel) {
        this.userService.editProfile(userModel).subscribe((responseModel: UserForEditModel) => {
            console.log(responseModel.executionResultMessage);
            this.userModel = responseModel;
        });
    }
}
