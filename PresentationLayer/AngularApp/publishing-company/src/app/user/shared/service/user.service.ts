import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserForEditModel } from '../models/user-for-edit.model';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) {
    }
    baseurl = environment.baseURL + 'User/';

    getUser() {
        const url = this.baseurl + 'GetUser';
        return this.http.get(url);
    }

    editProfile(userForEditModel: UserForEditModel): Observable<any> {
        const url = this.baseurl + 'EditProfile/';
        return this.http.post(url, userForEditModel);
    }
}
