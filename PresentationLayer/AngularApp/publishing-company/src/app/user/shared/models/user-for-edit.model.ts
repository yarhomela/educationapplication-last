export interface UserForEditModel {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword: string;
    executionResultMessage: string;
}