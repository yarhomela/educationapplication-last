export interface UserForEditByAdminModel {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    isEdited: boolean;
    resultMessage: string;
}
