export interface UserFieldsModel {
    id: string;
    userName: string;
    email: string;
    lockout: boolean;
}
