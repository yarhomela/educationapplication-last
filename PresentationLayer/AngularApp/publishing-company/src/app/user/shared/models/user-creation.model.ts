export interface UserCreationModel {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    passwordConfirm: string;
}
