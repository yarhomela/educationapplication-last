export interface RemoveUserResponseModel {
    isRemoved: boolean;
    resultMessage: string;
}
