import { PageInfoModel } from 'src/app/admin/shared/models/page-info.model';
import { UserFieldsModel } from './user-fields.model';

export interface UserViewModel {
    userFieldsModels: UserFieldsModel[];
    pageInfo: PageInfoModel;
}
