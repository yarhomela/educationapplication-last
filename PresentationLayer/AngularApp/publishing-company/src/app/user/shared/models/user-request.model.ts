import { UserSortingEnum } from 'src/app/shared/enums/user-sorting.enum';

export interface UserRequestModel {
    page: number;
    pageSize: number;
    userSorting: UserSortingEnum;
}
