import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './account/components/sign-in/sign-in.component';
import { SignUpComponent } from './account/components/sign-up/sign-up.component';
import { ForgotPassComponent } from './account/components/forgot-pass/forgot-pass.component'
import { PrintEditionModelComponent } from './printing-edition/components/print-edition-model/print-edition-model.component';
import { PrintEditionReadComponent } from './printing-edition/components/print-edition-read/print-edition-read.component';
import { UserProfileComponent } from './user/components/user-profile.component/user-profile.component';
import { UserOrdersComponent } from './order/components/user-order-list/user-order-list.component';
import { OrderManagementComponent } from './admin/components/order-managment/order-managment.component';
import { UserManagementComponent } from './admin/components/user-management/user-management.component';
import { ProductManagementComponent } from './admin/components/product-management/product-management.component';
import { AuthorManagementComponent } from './admin/components/author-management/author-management.component';
import { AdminGuard } from './shared/guards/admin.guard';


const routes: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'forgot-pass', component: ForgotPassComponent },
  { path: 'print-edition-model', component: PrintEditionModelComponent },
  { path: 'print-edition-read/:printId', component: PrintEditionReadComponent },
  { path: 'user-profile', component: UserProfileComponent },
  { path: 'user-order', component: UserOrdersComponent },
  { path: 'order-managment', component: OrderManagementComponent, canActivate: [AdminGuard] },
  { path: 'users-managment', component: UserManagementComponent, canActivate: [AdminGuard] },
  { path: 'product-management', component: ProductManagementComponent, canActivate: [AdminGuard] },
  { path: 'author-management', component: AuthorManagementComponent, canActivate: [AdminGuard] },
  { path: '**', component: PrintEditionModelComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
