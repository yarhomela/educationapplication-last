import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CartPopupComponent } from '../order/components/cart-popup/cart-popup.component';
import { AccountService } from '../account/shared/services/account.service';
import { PrintEditionRequestModel } from '../printing-edition/shared/models/print-edition-receive.model';
import { MessageService } from '../shared/services/observable.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() list: PrintEditionRequestModel[];
  subscription: Subscription;
  countCartItems: number;
  userName: string;
  userRole: string;
  isMenu = false;
  constructor(public dialog: MatDialog, private accountService: AccountService, private messageService: MessageService) {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.countCartItems = +localStorage.getItem('cart-count');
      this.countCartItems = +message.count;
      localStorage.setItem('cart-count', this.countCartItems.toString());
    }
    );
  }
  ngOnInit() {
    this.userName = localStorage.getItem('user-name');
    this.userRole = localStorage.getItem('user-role');
    this.countCartItems = +localStorage.getItem('cart-count');
  }
  openDialog(): void {
    const cart = this.dialog.open(CartPopupComponent);
    cart.afterClosed().subscribe(x => {
      this.ngOnInit();
    });
  }
  logOut() {
    this.accountService.signOut().subscribe(result => {
      console.log(result);
    });
  }
}
