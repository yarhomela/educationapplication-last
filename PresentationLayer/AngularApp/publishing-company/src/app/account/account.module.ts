import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { HttpClientModule } from '@angular/common/http';
import { AccountService } from './shared/services/account.service';
import { RouterModule, Router } from '@angular/router';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForgotPassComponent } from './components/forgot-pass/forgot-pass.component';

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule, Router],
    exports: [SignInComponent, SignUpComponent, ForgotPassComponent],
    declarations: [SignInComponent, SignUpComponent, ForgotPassComponent],
    providers: [HttpClientModule, AccountService],
})
export class AccountModule {

}
