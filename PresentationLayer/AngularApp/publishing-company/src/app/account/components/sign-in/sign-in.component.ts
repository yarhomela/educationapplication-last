import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { SignInModel } from '../../shared/models/sign-in.model';
import { ResponseTokenModel } from '../../shared/models/response-token.model';
import { Router } from '@angular/router';
import { ChooseCurrencyModel } from 'src/app/shared/models/choose-currency.model';
import { ChooseCurrencyService } from 'src/app/shared/services/choose-currency.service';
import { EnumToListHelper } from 'src/app/shared/helpers/enum-to-list.service';
import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  private router: Router;
  signInModel: SignInModel;
  tokenModel: ResponseTokenModel;
  currency: number;
  requestDone: boolean;
  currencySelection: Array<number>;
  chooseCurrencyModel: ChooseCurrencyModel;
  constructor(
    private accountService: AccountService, route: Router,
    private chooseCurrencyService: ChooseCurrencyService,
    private enumToListHelper: EnumToListHelper) {
    this.router = route;
  }
  ngOnInit() {
    this.signInModel = {
      email: '',
      password: '',
      isPersistent: false
    };
    this.chooseCurrencyModel = {
      currency: 1,
      currencyToDollar: 1
    };
    this.currencySelection = this.enumToListHelper.generateListNumber(CurrencyEnum);
  }
  signIn(model: SignInModel) {
    this.accountService.signIn(model).subscribe((responseModel: ResponseTokenModel) => {
      this.tokenModel = responseModel;
      this.requestDone = true;
      if (responseModel.isAuth) {
        localStorage.setItem('access-token', responseModel.accessToken);
        localStorage.setItem('refresh-token', responseModel.refreshToken);
        localStorage.setItem('user-name', responseModel.userName);
        localStorage.setItem('user-role', responseModel.userRole);
        console.log(responseModel.executionResultMessage);
        return;
      }
      this.tokenModel.isAuth = false;
      console.log(responseModel.executionResultMessage);
    });
  }
  chooseCurrency(currency: number) {
    let model = this.chooseCurrencyModel;
    model = this.chooseCurrencyService.getCurrencyParameters(currency);
    localStorage.setItem('currency', model.currency.toString());
    localStorage.setItem('currency-coeff', model.currencyToDollar.toString());
    this.router.navigate(['/view-print']);
  }
}
