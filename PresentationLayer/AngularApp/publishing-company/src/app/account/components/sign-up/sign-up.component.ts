import { Component } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { SignUpModel } from '../../shared/models/sign-up.model';
import { SignUpResponseModel } from '../../shared/models/sign-up-response.model';

@Component({
    selector: 'sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['sign-up.component.scss']
})
export class SignUpComponent {
    signUpModel: SignUpModel;
    response: string;
    isSignUp: boolean;
    constructor(private accountService: AccountService) { }
    onInit() {
        this.signUpModel = {
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        };
    }
    signUp(model: SignUpModel) {
        this.accountService.signUp(model).subscribe((responseModel: SignUpResponseModel) => {
            console.log(responseModel.resultMessage);
            this.response = responseModel.resultMessage;
            this.isSignUp = responseModel.isSignUp;
        });
    }
}
