import { Component } from '@angular/core';
import { AccountService } from '../../shared/services/account.service';
import { ForgotPassResponseModel } from '../../shared/models/forgot-password-response.model';


@Component({
    selector: 'forgot-pass',
    templateUrl: './forgot-pass.component.html',
    styleUrls: ['./forgot-pass.component.scss']
})
export class ForgotPassComponent {
    email: string;
    isExistingUser: boolean;
    requestDone: boolean;
    constructor(private accountService: AccountService) { }
    forgotPass(email: string) {
        this.accountService.forgotPass(email).subscribe((model: ForgotPassResponseModel) => {
            console.log(model.resultMessage);
            this.isExistingUser = model.isExistingUser;
            this.requestDone = true;
        });
    }
}
