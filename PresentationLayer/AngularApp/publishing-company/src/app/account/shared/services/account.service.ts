import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SignInModel } from '../models/sign-in.model';
import { SignUpModel } from '../models/sign-up.model';
import { RefreshTokenModel } from '../models/refresh-token.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: HttpClient) { }
  baseurl = environment.baseURL + 'Account/';
  result: string;
  public getToken(): string {
    return localStorage.getItem('access-token');
  }
  signIn(signInModel: SignInModel): Observable<any> {
    const url = this.baseurl + 'SignIn/';
    return this.http.post(url, signInModel);
  }
  signUp(signUpModel: SignUpModel): Observable<any> {
    const url = this.baseurl + 'SignUp/';
    return this.http.post(url, signUpModel);
  }
  forgotPass(email: string): Observable<any> {
    const url = this.baseurl + 'ForgotPassword/' + email;
    return this.http.get(url);
  }
  signOut() {
    const url = this.baseurl + 'SignOut/';
    localStorage.removeItem('access-token');
    localStorage.removeItem('refresh-token');
    localStorage.removeItem('user-name');
    localStorage.removeItem('user-role');
    localStorage.removeItem('order-items');
    localStorage.removeItem('message');
    localStorage.removeItem('currency');
    localStorage.removeItem('currency-coeff');
    localStorage.removeItem('cart-count');
    return this.http.get(url);
  }
  refresh(refreshTokenModel: RefreshTokenModel): Observable<any> {
    const url = this.baseurl + 'Refresh/';
    return this.http.post(url, refreshTokenModel);
  }
}
