import { Injectable, OnInit } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccountService } from './account.service';
import { RefreshTokenModel } from '../models/refresh-token.model';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private router: Router;
  mySubscription: any;
  refreshTokenModel: RefreshTokenModel;
  constructor(public accountService: AccountService, route: Router) {
  this.router = route;
  this.refreshTokenModel = {
      accessToken: '',
      refreshToken: ''
  };
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.accountService.getToken()}`
      }
    });
    return next.handle(request).pipe(tap(() => { },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if ((err.status !== 401) && (err.status !== 403)) {
            return;
          }
          const requestModel = this.refreshTokenModel;
          requestModel.accessToken = localStorage.getItem('access-token');
          requestModel.refreshToken = localStorage.getItem('refresh-token');
          this.accountService.refresh(requestModel).subscribe((responseModel: RefreshTokenModel) => {
            if (!responseModel.isRefresh) {

              console.log(responseModel.executionResultMessage);
              return this.router.navigate(['/sign-in']);
            }

            localStorage.setItem('access-token', responseModel.accessToken);
            localStorage.setItem('refresh-token', responseModel.refreshToken);
            console.log(responseModel.executionResultMessage);
            location.reload();
          });
        }
      }));

  }
}
