export interface ForgotPassResponseModel {
    resultMessage: string;
    isExistingUser: boolean;
}
