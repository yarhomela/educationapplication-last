export interface SignUpResponseModel {
    resultMessage: string;
    isSignUp: boolean;
}
