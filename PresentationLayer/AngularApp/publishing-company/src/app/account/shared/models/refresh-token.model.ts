export interface RefreshTokenModel {
    accessToken: string;
    refreshToken: string;
    executionResultMessage?: string;
    isRefresh?: boolean;
}
