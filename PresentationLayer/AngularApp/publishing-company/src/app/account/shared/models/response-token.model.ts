export interface ResponseTokenModel {
    accessToken: string;
    refreshToken: string;
    userName: string;
    executionResultMessage: string;
    userRole: string;
    isAuth: boolean;
}
