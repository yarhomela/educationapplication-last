import { Component, OnInit } from '@angular/core';
import { PrintEditionService } from '../../shared/services/print-edition.service';
import { PrintEditionRequestModel } from '../../shared/models/print-edition-receive.model';
import { PrintEditionFieldsModel } from '../../shared/models/print-edition-fields.model';
import { PrintEditionsModel } from '../../shared/models/print-edition-view.model';
import { PageInfoModel } from 'src/app/admin/shared/models/page-info.model';
import { EnumToListHelper } from 'src/app/shared/helpers/enum-to-list.service';
import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';

@Component({
    selector: 'print-edition-model',
    templateUrl: './print-edition-model.component.html',
    styleUrls: ['./print-edition-model.component.scss']
})

export class PrintEditionModelComponent implements OnInit {
    printEditionList: PrintEditionFieldsModel[] = new Array();
    requestModel: PrintEditionRequestModel;
    pages: number[];
    pageInfo: PageInfoModel;
    currency: number;
    currencyCoeff: number;
    minPrice: number;
    maxPrice: number;
    sortingOptions: Array<number>;
    constructor(private printService: PrintEditionService, private enumToListHelper: EnumToListHelper) { }

    ngOnInit() {
        this.requestModel = {
            page: 0,
            pageSize: 6,
            printSorting: 0,
            typePrintEdit: 0,
            searchByTitle: '',
            minPrice: 0,
            maxPrice: 0
        };
        this.currency = +localStorage.getItem('currency');
        this.currencyCoeff = +localStorage.getItem('currency-coeff');
        if (this.currency === 0) {
            this.currency = 1;
            localStorage.setItem('currency', this.currency.toString());
            this.currencyCoeff = 1;
            localStorage.setItem('currency-coeff', this.currencyCoeff.toString());
        }
        this.requestModel.page = 1;
        this.getPrintEditions(this.requestModel);
        this.sortingOptions = this.enumToListHelper.generateListNumber(TypePrintEditEnum);
    }

    getPrintEditions(requestModel: PrintEditionRequestModel) {
        if (requestModel.maxPrice !== undefined) {
            requestModel.minPrice = requestModel.minPrice / this.currencyCoeff;
            requestModel.maxPrice = requestModel.maxPrice / this.currencyCoeff;
        }
        this.printService.getPrintEditions(requestModel).subscribe((responseModel: PrintEditionsModel) => {
            if (requestModel.maxPrice === undefined) {
                this.minPrice = responseModel.minPrice * this.currencyCoeff;
                this.maxPrice = responseModel.maxPrice * this.currencyCoeff;
            }
            this.pageInfo = responseModel.pageInfo;
            this.printEditionList = responseModel.printingEditionFieldsModels;
            this.requestModel.minPrice = responseModel.minPrice * this.currencyCoeff;
            this.requestModel.maxPrice = responseModel.maxPrice * this.currencyCoeff;
            this.pages = new Array();
            for (let i = 1; i <= responseModel.pageInfo.totalPages; i++) {
                this.pages.push(i);
            }
        });
    }
    pagination(pageNumber: number) {
        this.requestModel.page = pageNumber;
        this.getPrintEditions(this.requestModel);
    }
}
