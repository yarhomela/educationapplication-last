import { Component, OnInit } from '@angular/core';
import { PrintEditionService } from '../../shared/services/print-edition.service';
import { ActivatedRoute } from '@angular/router';
import { PrintEditionModel } from '../../shared/models/print-edition.model';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageService } from 'src/app/shared/services/observable.service';

@Component({
    selector: 'print-edition-read',
    templateUrl: './print-edition-read.component.html',
    styleUrls: ['./print-edition-read.component.scss']
})

export class PrintEditionReadComponent implements OnInit {
    numbers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    cartCount: number;
    printId: string;
    printEdition: PrintEditionModel;
    cartList: PrintEditionModel[] = new Array();
    currency: number;
    currencyCoeff: number;
    constructor(
        private printService: PrintEditionService, private snackBar: MatSnackBar, private messageService: MessageService,
        private activateRoute: ActivatedRoute, public dialog: MatDialog ) { this.printId = this.activateRoute.snapshot.params['printId']; }
    ngOnInit() {
        this.printEdition = {
            id: '',
            title: '',
            description: '',
            price: 0,
            typePrintEdit: 0,
            authorId: '',
            quantity: 0
        };
        this.cartCount = +localStorage.getItem('cart-count');
        this.setCartCount(this.cartCount);
        this.currency = +localStorage.getItem('currency');
        this.currencyCoeff = +localStorage.getItem('currency-coeff');
        this.readPrintEdition(this.printId);
    }

    setCartCount(count: number): void {
        this.messageService.sendCartCount(count);
    }

    readPrintEdition(printId: string) {
        this.printService.readPrintEdition(printId).subscribe((responseModel: PrintEditionModel) => {
            this.printEdition = responseModel;
            this.printEdition.quantity = 1;
            this.printEdition.price = this.printEdition.price * this.currencyCoeff;
        });
    }
    setItemToCart(printEdition: PrintEditionModel) {
        const list = JSON.parse(localStorage.getItem('order-items'));
        if (list != null) {
            this.cartList = list;
        }
        printEdition.id = this.printId;
        printEdition.currency = this.currency;
        this.cartList.push(printEdition);
        const parsedCartItemsList = JSON.stringify(this.cartList);
        localStorage.setItem('order-items', parsedCartItemsList);
        const textMessage = 'Print edition added to Cart!';
        this.snackBar.open(textMessage, 'Ok', {
            duration: 2000,
        });
        this.cartCount++;
        this.setCartCount(this.cartCount);
    }
}
