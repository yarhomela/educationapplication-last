import { PageInfoModel } from '../../../admin/shared/models/page-info.model';
import { PrintEditionFieldsModel } from './print-edition-fields.model';

export interface PrintEditionsModel {
    printingEditionFieldsModels: PrintEditionFieldsModel[];
    pageInfo: PageInfoModel;
    minPrice: number;
    maxPrice: number;
}
