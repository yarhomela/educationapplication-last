import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';
import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';
import { AuthorModel } from 'src/app/admin/shared/models/author.model';

export interface PrintEditionForEditModel {
    id: string;
    title: string;
    description: string;
    price: number;
    currency: CurrencyEnum;
    typePrintEdit: TypePrintEditEnum;
    authorId: string;
    authors?: AuthorModel[];
    resultMessage?: string;
    isUpdated?: boolean;
}
