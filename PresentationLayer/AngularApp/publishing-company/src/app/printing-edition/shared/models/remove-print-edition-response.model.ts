export interface RemovePrintEditionResponseModel {
    isRemoved: boolean;
    resultMessage: string;
}
