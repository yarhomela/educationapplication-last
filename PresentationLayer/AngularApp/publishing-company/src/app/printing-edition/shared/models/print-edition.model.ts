import { TypePrintEditEnum } from 'src/app/shared/enums/type-print-edit.enum';
import { CurrencyEnum } from 'src/app/shared/enums/currency.enum';

export interface PrintEditionModel {
    id: string;
    title: string;
    description: string;
    price: number;
    currency?: CurrencyEnum;
    typePrintEdit: TypePrintEditEnum;
    authorName?: string;
    authorId: string;
    quantity: number;
}
