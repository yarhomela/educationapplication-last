import { CurrencyEnum } from '../../../shared/enums/currency.enum';
import { TypePrintEditEnum } from '../../../shared/enums/type-print-edit.enum';

export interface PrintEditionFieldsModel {
    id: string;
    title: string;
    description: string;
    typePrintEdit: TypePrintEditEnum;
    currency: CurrencyEnum;
    authorName: string;
    price: number;
}
