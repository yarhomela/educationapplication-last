import { TypePrintEditEnum } from '../../../shared/enums/type-print-edit.enum';
import { PrintSortingEnum } from '../../../shared/enums/print-sorting.enum';

export interface PrintEditionRequestModel {
        page: number;
        pageSize: number;
        printSorting: PrintSortingEnum;
        typePrintEdit: TypePrintEditEnum;
        searchByTitle: string;
        minPrice: number;
        maxPrice: number;
}
