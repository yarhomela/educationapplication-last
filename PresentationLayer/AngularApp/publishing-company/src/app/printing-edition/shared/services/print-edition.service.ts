import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PrintEditionRequestModel } from '../models/print-edition-receive.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrintEditionService {
  constructor(private http: HttpClient) {
  }
  baseurl = environment.baseURL + 'PrintingEdition/';
  getPrintEditions(printEditionRequestModel: PrintEditionRequestModel): Observable<any> {
    const url = this.baseurl + 'GetPrintEditions';
    return this.http.post(url, printEditionRequestModel);
  }
  readPrintEdition(printId: string): Observable<any> {
    const url = this.baseurl + 'ReadPrintEdition/' + printId;
    return this.http.get(url);
  }
}
