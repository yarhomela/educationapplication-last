import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { PrintEditionService } from './shared/services/print-edition.service';

import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { PrintEditionModelComponent } from './components/print-edition-model/print-edition-model.component';
import { PrintEditionReadComponent } from './components/print-edition-read/print-edition-read.component';
import { EnumToListHelper } from '../shared/helpers/enum-to-list.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MessageService } from '../shared/services/observable.service';


@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule, MatDialogModule, MatSnackBarModule],
    exports: [PrintEditionModelComponent, PrintEditionReadComponent],
    declarations: [PrintEditionModelComponent, PrintEditionReadComponent],
    providers: [HttpClientModule, PrintEditionService, EnumToListHelper, MatDialogModule, MatDialogRef, MatSnackBar, MessageService]
})
export class PrintingEditionModule {

}
