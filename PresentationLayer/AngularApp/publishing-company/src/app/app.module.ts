import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './account/shared/services/token.interceptor';

import { HeaderComponent } from 'src/app/header/header.component';
import { FooterComponent } from './footer/footer.component';

import { SignInComponent } from './account/components/sign-in/sign-in.component';
import { SignUpComponent } from './account/components/sign-up/sign-up.component';
import { ForgotPassComponent } from './account/components/forgot-pass/forgot-pass.component'
import { PrintEditionModelComponent } from './printing-edition/components/print-edition-model/print-edition-model.component';
import { PrintEditionReadComponent } from './printing-edition/components/print-edition-read/print-edition-read.component';
import { CartPopupComponent } from './order/components/cart-popup/cart-popup.component';
import { UserProfileComponent } from './user/components/user-profile.component/user-profile.component';
import { UserOrdersComponent } from './order/components/user-order-list/user-order-list.component';
import { OrderManagementComponent } from './admin/components/order-managment/order-managment.component';
import { UserManagementComponent } from './admin/components/user-management/user-management.component';

import { UserPopupComponent } from './admin/components/user-popup/user-popup.component';
import { ProductManagementComponent } from './admin/components/product-management/product-management.component';
import { PrintEditionPopupComponent } from './admin/components/print-add-popup/print-edition-popup.component';
import { AuthorManagementComponent } from './admin/components/author-management/author-management.component';
import { StripeCheckoutModule } from 'ng-stripe-checkout';


import { MessagePopupComponent } from './message-popup/message-popup.component';
import { AuthorPopupComponent } from './admin/components/author-popup/author-popup.component';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

import { PrintCategoryPipe } from './shared/pipes/print-category.pipe';
import { PaidStatusPipe } from './shared/pipes/paid-status.pipe';
import { CurrencySelectPipe } from './shared/pipes/currency-select.pipe';
import { CustomCurrencyPipe } from './shared/pipes/currency.pipe';
import { AdminGuard } from './shared/guards/admin.guard';



@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    HeaderComponent,
    FooterComponent,
    ForgotPassComponent,
    PrintEditionModelComponent,
    PrintEditionReadComponent,
    CartPopupComponent,
    UserProfileComponent,
    UserOrdersComponent,
    OrderManagementComponent,
    UserManagementComponent,
    UserPopupComponent,
    ProductManagementComponent,
    PrintEditionPopupComponent,
    AuthorManagementComponent,
    AuthorPopupComponent,
    MessagePopupComponent,
    CustomCurrencyPipe,
    PrintCategoryPipe,
    PaidStatusPipe,
    CurrencySelectPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatIconModule,
    StripeCheckoutModule,
    MatSnackBarModule
  ],
  providers:
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
      }, MatSnackBar, AdminGuard
    ],
  bootstrap: [AppComponent],
  entryComponents: [CartPopupComponent, UserPopupComponent, PrintEditionPopupComponent, AuthorPopupComponent, MessagePopupComponent]
})
export class AppModule { }
