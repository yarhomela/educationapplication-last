import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyEnum } from '../enums/currency.enum';
import { EnumToListHelper } from '../helpers/enum-to-list.service';

@Pipe({
    name: 'customcurrency'
})

export class CustomCurrencyPipe implements PipeTransform {
    constructor(private enumToListHepler: EnumToListHelper) { }
    transform(value: number, currency: CurrencyEnum): string {
        const enumPropertiesList = this.enumToListHepler.generateListString(CurrencyEnum);
        return enumPropertiesList[currency] + ' ' + value;
    }
}