import { Pipe, PipeTransform } from "@angular/core";
import { StatusEnum } from 'src/app/shared/enums/status.enum';

@Pipe({
    name: 'status'
})

export class PaidStatusPipe implements PipeTransform {
    transform(status: StatusEnum, args?: any): string {
        if (status === 1){
            return 'Unpaid';
        }
        if (status === 2){
            return 'Paid';
        }
        return 'None';
    }
}
