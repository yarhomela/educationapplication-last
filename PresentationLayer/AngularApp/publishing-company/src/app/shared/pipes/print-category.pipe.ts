import { Pipe, PipeTransform } from '@angular/core';
import { TypePrintEditEnum } from '../enums/type-print-edit.enum';
import { EnumToListHelper } from '../helpers/enum-to-list.service';

@Pipe({
    name: 'category'
})

export class PrintCategoryPipe implements PipeTransform {
    constructor(private enumToListHepler: EnumToListHelper) { }
    transform(typePrint: TypePrintEditEnum, args?: any): string {
        const enumPropertiesList = this.enumToListHepler.generateListString(TypePrintEditEnum);
        return enumPropertiesList[typePrint];
    }
}
