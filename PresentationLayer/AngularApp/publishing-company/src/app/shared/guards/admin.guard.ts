import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

export class AdminGuard implements CanActivate {

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const userRole = localStorage.getItem('user-role');
        if (userRole === 'admin') {
            return confirm('Please, confirm route to administration page');
        }
        return false;
    }
}
