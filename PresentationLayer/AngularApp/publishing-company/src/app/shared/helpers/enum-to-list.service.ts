import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class EnumToListHelper {
    generateListString(customEnum: any) {
        const array: Array<string> = new Array();

        for (const n in customEnum) {
            if (typeof customEnum[n] === 'number') {
                array.push(n);
            }
        }
        return array;
    }
    generateListNumber(customEnum: any) {
        const arrayLength = this.generateListString(customEnum).length;
        const list = new Array<number>();
        for (let i = 0; i < arrayLength; i++) {
            list.push(i);
        }
        return list;
    }
}
