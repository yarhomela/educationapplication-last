export enum TypePrintEditEnum {
    None = 0,
    Book = 1,
    Magazines = 2,
    Newspapers = 3
}
