export enum UserSortingEnum {
    None = 0,
    byName = 1,
    byEmail = 2
}
