import { CurrencyEnum } from '../enums/currency.enum';

export interface ChooseCurrencyModel {
    currency: CurrencyEnum;
    currencyToDollar: number;
}
