import { Injectable } from '@angular/core';
import { CurrencyEnum } from '../enums/currency.enum';
import { ChooseCurrencyModel } from '../models/choose-currency.model';

@Injectable({
  providedIn: 'root'
})
export class ChooseCurrencyService {
  chooseCurrencyModel: ChooseCurrencyModel;
  getCurrencyParameters(currency: number): ChooseCurrencyModel {
    const model = this.chooseCurrencyModel;
    model.currency = currency;
    if ((CurrencyEnum.USD === currency) || (CurrencyEnum.None === currency)) {
      model.currencyToDollar = 1;
    }
    if (CurrencyEnum.EUR === currency) {
      model.currencyToDollar = 0.897;
    }
    if (CurrencyEnum.GBP === currency) {
      model.currencyToDollar = 0.747;
    }
    if (CurrencyEnum.CHF === currency) {
      model.currencyToDollar = 0.983;
    }
    if (CurrencyEnum.JPY === currency) {
      model.currencyToDollar = 109.385;
    }
    if (CurrencyEnum.UAH === currency) {
      model.currencyToDollar = 23.59;
    }
    return model;
  }
}
