import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MessageService {
    private subject = new Subject<any>();

    sendCartCount(cartCount: number) {
        this.subject.next({ count: cartCount });
    }
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
