﻿using EducationApp.BusinessLogicLayer.DependencyInjection;
using EducationApp.PresentationLayer.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using EducationApp.PresentationLayer.Helpers;
using System;
using EducationApp.BusinessLogicLayer.Models.EmailHelper;
using EducationApp.BusinessLogicLayer.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using EducationApp.BusinessLogicLayer.Models.Stripe;
using AutoMapper;
using EducationApp.BusinessLogicLayer.Services;

namespace PresentationLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            const string jwtSchemeName = "JwtBearer";
            services.AddControllers();
            services.ConfigureIdentity(Configuration);
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            services.AddCors(options => options.AddPolicy("AllowAll", builder => builder
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowAnyMethod())
                    );
            services.SetDependencies();
            services.AddOptions();
            services.AddScoped<EmailHelper>();
            services.Configure<EmailSettingsModel>(Configuration.GetSection("EmailSettings"));
            services.Configure<StripeSettingsModel>(Configuration.GetSection("StripeSettings"));
            services.AddScoped<JwtHelper>();
            services
        .AddAuthentication(options => {
            options.DefaultAuthenticateScheme = jwtSchemeName;
            options.DefaultChallengeScheme = jwtSchemeName;
        })
        .AddJwtBearer(jwtSchemeName, jwtBearerOptions => {
            jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = JwtHelper.ISSUER,
                ValidateAudience = true,
                ValidAudience = JwtHelper.AUDIENCE,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = JwtHelper.GetSymmetricSecurityKey(),
                ClockSkew = TimeSpan.FromSeconds(5)
            };
        });
            services.AddControllersWithViews();
        }
        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();

            app.UseCors("AllowAll");
            app.UseHttpsRedirection();
            app.UseSwagger();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAreaControllerRoute(
                   name: "admin_route",
                   areaName: "Administration",
                   pattern: "{area:exists}/",
                   defaults: "default");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "/");
                endpoints.MapControllers();
            });
        }
    }
}
