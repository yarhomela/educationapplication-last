﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using EducationApp.BusinessLogicLayer.Models.Author;
using EducationApp.BusinessLogicLayer.Models.Order;
using EducationApp.BusinessLogicLayer.Models.PrintingEdition;
using EducationApp.BusinessLogicLayer.Models.User;
using EducationApp.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EducationApp.PresentationLayer.Areas.Administration.Controllers
{
    [Area("Administration")]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IAuthorService _authorService;
        private readonly IOrderService _orderService;
        private readonly IPaymentService _paymentService;
        private readonly IPrintingEditionService _printingEditionService;
        private readonly IUserService _userService;
        public AdminController(IAuthorService authorService, IOrderService orderService, IPaymentService paymentService,
            IPrintingEditionService printingEditionService, IUserService userService)
        {
            _authorService = authorService;
            _orderService = orderService;
            _paymentService = paymentService;
            _printingEditionService = printingEditionService;
            _userService = userService;
        }
        [HttpGet("AddAuthor/{authorName}")]
        public async Task<IActionResult> AddAuthor( [Required] string authorName)
        {
            if(ModelState.IsValid)
            {
                var result = await _authorService.Create(authorName);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpGet("GetAuthor/{authorID}")]
        public async Task<IActionResult> GetAuthor( [Required] string authorID)
        {
            if (ModelState.IsValid)
            {
                var result = await _authorService.GetAuthor(authorID);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpPost("EditAuthor")]
        public async Task<IActionResult> EditAuthor(EditAuthorModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await _authorService.Edit(model);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpGet("RemoveAuthor/{authorId}")]
        public async Task<IActionResult> RemoveAuthor( [Required] string authorId)
        {
            if (ModelState.IsValid)
            {
                var responseModel = await _authorService.Remove(authorId);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpPost("GetAuthorsWithPrintEdition")]
        public async Task<IActionResult> GetAuthorList(AuthorRequestModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _authorService.GetAuthorsWithPrintEdition(model);
                return Ok(responseModel);
            }
            return BadRequest();
        }


        [HttpPost("GetAdminOrders")]
        public async Task<IActionResult> AdminOrderListModel(OrderListRequestModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _orderService.GetOrders(model, User.Identity.Name, true);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("FullRemoveOrder/{orderId}")]
        public async Task<IActionResult> FullRemoveOrder( [Required] string orderId)
        {
            if(ModelState.IsValid)
            {
                var result = await _orderService.FullRemove(orderId);
                return Ok(result);
            }
            return BadRequest();

        }
        [HttpGet("FullRemoveOrderItem/{orderItemId}")]
        public async Task<IActionResult> FullRemoveOrderItem( [Required] string orderItemId)
        {
            if(ModelState.IsValid)
            {
                var result = await _orderService.FullRemoveOrderItem(orderItemId);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpGet("FullRemovePayment/{paymentId}")]
        public async Task<IActionResult> PaymentFullRemove( [Required] string paymentId)
        {
            if(ModelState.IsValid)
            {
                var result = await _paymentService.FullRemove(paymentId);
                return Ok(result);
            }
            return BadRequest();
        }


        [HttpPost("AddPrintEdition")]
        public async Task<IActionResult> AddPrintEdition(PrintEditionModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await _printingEditionService.Create(model);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpPost("EditPrintEdition")]
        public async Task<IActionResult> EditPrintEdition(PrintEditionForEditModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _printingEditionService.Edit(model);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("GetPrintEdition/{printEditionID}")]
        public async Task<IActionResult> GetPrintEdition( [Required] string printEditionID)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _printingEditionService.Read(printEditionID);
                return Ok(responseModel);
            }
            return BadRequest();
        }

        [HttpPost("GetPrintEditions")]
        public async Task<IActionResult> GetPrintEditions(PrintEditionRequestModel model)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _printingEditionService.GetPrintEditions(model);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("RemovePrintEdition/{printId}")]
        public async Task<IActionResult> RemovePrintEdition( [Required] string printId)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _printingEditionService.Remove(printId);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("FullRemovePrintEdition/{printEditionId}")]
        public async Task<IActionResult> FullRemovePrintEdition( [Required] string printEditionId)
        {
            if(ModelState.IsValid)
            {
                var result = await _printingEditionService.FullRemove(printEditionId);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpGet("GetAuthors")]
        public async Task<IActionResult> GetAuthors()
        {
            var authorList = await _authorService.GetAuthors();
            return Ok(authorList);
        }



        [HttpPost("AddUser")]
        public async Task<IActionResult> AddUser(UserCreationModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await _userService.Create(model);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpGet("RemoveUser/{userId}")]
        public async Task<IActionResult> RemoveUser( [Required] string userId)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _userService.Remove(userId);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("UserSetLockout/{userId}/{lockoutEnabled}")]
        public async Task<IActionResult> UserSetLockout([Required] string userId, bool lockoutEnabled)
        {
            if(ModelState.IsValid)
            {
                var result = await _userService.SetLockout(userId, lockoutEnabled);
                return Ok(result);
            }
            return BadRequest();
        }
        [HttpPost("GetUsers")]
        public async Task<IActionResult> GetUsers(UserRequestModel model)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            var responseModel = await _userService.GetUsers(model);
            if(ModelState.IsValid)
            {
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpGet("GetUserForAdmin/{userID}")]
        public async Task<IActionResult> GetUser( [Required] string userID)
        {
            if(ModelState.IsValid)
            {
                var responseModel = await _userService.GetUserForAdmin(userID);
                return Ok(responseModel);
            }
            return BadRequest();
        }
        [HttpPost("EditUser")]
        public async Task<IActionResult> EditUser(UserForEditByAdminModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var responseModel = await _userService.EditByAdmin(model);
            if (ModelState.IsValid)
            {
                return Ok(responseModel);
            }
            return BadRequest();
        }
    }
}