﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EducationApp.DataAccessLayer.Migrations
{
    public partial class AddreferencePrintEditioninOrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrintingEditions_OrderItems_OrderItemId",
                table: "PrintingEditions");

            migrationBuilder.DropIndex(
                name: "IX_PrintingEditions_OrderItemId",
                table: "PrintingEditions");

            migrationBuilder.DropColumn(
                name: "OrderItemId",
                table: "PrintingEditions");

            migrationBuilder.AddColumn<string>(
                name: "PrintingEditionId",
                table: "OrderItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_PrintingEditionId",
                table: "OrderItems",
                column: "PrintingEditionId",
                unique: true,
                filter: "[PrintingEditionId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItems_PrintingEditions_PrintingEditionId",
                table: "OrderItems",
                column: "PrintingEditionId",
                principalTable: "PrintingEditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItems_PrintingEditions_PrintingEditionId",
                table: "OrderItems");

            migrationBuilder.DropIndex(
                name: "IX_OrderItems_PrintingEditionId",
                table: "OrderItems");

            migrationBuilder.DropColumn(
                name: "PrintingEditionId",
                table: "OrderItems");

            migrationBuilder.AddColumn<string>(
                name: "OrderItemId",
                table: "PrintingEditions",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrintingEditions_OrderItemId",
                table: "PrintingEditions",
                column: "OrderItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrintingEditions_OrderItems_OrderItemId",
                table: "PrintingEditions",
                column: "OrderItemId",
                principalTable: "OrderItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
