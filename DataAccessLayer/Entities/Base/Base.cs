﻿using System;

namespace EducationApp.DataAccessLayer.Entities.Base
{
    public class Base
    {
        public string Id { get; set; }
        public bool IsRemoved { get; set; }
        public DateTime CreationData { get; set; }

        public Base()
        {
            Id = Guid.NewGuid().ToString();
            CreationData = DateTime.Now;
        }
    }
}
