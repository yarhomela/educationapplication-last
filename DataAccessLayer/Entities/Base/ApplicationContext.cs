﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EducationApp.DataAccessLayer.Entities.Base
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PrintingEdition> PrintingEditions { get; set; }
        

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OrderItem>()
            .HasOne(p => p.Order)
            .WithMany(t => t.OrderItems)
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey(bc => bc.OrderId);

            modelBuilder.Entity<Order>()
                .HasOne(h => h.Payment)
                .WithOne(w => w.Order)
                .HasForeignKey<Payment>(k => k.OrderId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<AuthorInPrintingEdition>()
                .HasOne(bc => bc.PrintingEdition)
                .WithMany(b => b.AuthorInPrintingEditions)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(bc => bc.PrintingEditionId);

            modelBuilder.Entity<AuthorInPrintingEdition>()   
                .HasOne(bc => bc.Author)
                .WithMany(c => c.AuthorInPrintingEditions)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(bc => bc.AuthorId);

            modelBuilder.Entity<Order>().HasIndex(e => e.UserId).IsUnique(false);
            modelBuilder.Entity<OrderItem>().HasIndex(e => e.PrintingEditionId).IsUnique(false);
        }
    }
}
