﻿
using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.DataAccessLayer.Entities
{
    public class OrderItem : Base.Base
    {
        public int Quantity { get; set; }
        public string PrintingEditionId { get; set; }
        public virtual PrintingEdition PrintingEdition { get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
        public string OrderId { get; set; }
        public virtual Order Order { get; set; }

    }
}
