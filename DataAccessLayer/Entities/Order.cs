﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities
{
    public class Order : Base.Base
    {
        public string Description { get; set; }
        public int Amount { get; set; }
        public virtual User User { get; set; } 
        public string UserId { get; set; }
        public Status Status { get; set; }
        public Currency Currency { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}
