﻿using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities
{
    public class Author : Base.Base
    {
        public string Name { get; set; }
        public virtual ICollection<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }

    }
}
