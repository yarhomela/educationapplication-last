﻿using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.DataAccessLayer.Entities.DTO
{
    public class GetPrintEditionRequestDTO
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public PrintSorting PrintSorting { get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
        public string SearchByTitle { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
    }
}
