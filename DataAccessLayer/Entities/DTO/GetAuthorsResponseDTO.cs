﻿using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities.DTO
{
    public class GetAuthorsResponseDTO
    {
        public IList<AuthorDTO> Authors { get; set; }
        public int TotalItems { get; set; }
    }
}
