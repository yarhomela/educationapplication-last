﻿using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities.DTO
{
    public class AuthorDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> Products { get; set; }
    }
}
