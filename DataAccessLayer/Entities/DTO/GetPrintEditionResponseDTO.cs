﻿using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities.DTO
{
    public class GetPrintEditionResponseDTO
    {
        public IList<PrintingEdition> PrintingEditions { get; set; }
        public int TotalItems { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
    }
}
