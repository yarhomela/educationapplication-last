﻿
using System;

namespace EducationApp.DataAccessLayer.Entities
{
    public class Payment : Base.Base
    {
        public long Amount { get; set; }
        public string BalanceTransactionId { get; set; }
        public string Currency { get; set; }
        public DateTime DateTimeCreatedUTC { get; set; }
        public string StripeChargeId { get; set; }
        public string StripeCustomerId { get; set; }
        public virtual Order Order { get; set; }
        public string OrderId { get; set; }
    }
}
