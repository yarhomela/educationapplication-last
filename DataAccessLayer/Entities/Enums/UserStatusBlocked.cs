﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
     public enum UserStatusBlocked
    {
        Active = 0,
        Blocked = 1
    }
}
