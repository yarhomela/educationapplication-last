﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
    public enum Status {
        None = 0,
        Unpaid = 1,
        Paid = 2,
    }
}
