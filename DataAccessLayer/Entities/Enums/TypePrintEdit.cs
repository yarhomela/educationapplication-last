﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
    public enum TypePrintEdit {
        None = 0,
        Book = 1, 
        Magazines = 2, 
        Newspapers = 3 
    }
}
