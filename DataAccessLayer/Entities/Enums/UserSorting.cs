﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
    public enum UserSorting
    {
        None = 0,
        byName = 1,
        byEmail = 2
    }
}
