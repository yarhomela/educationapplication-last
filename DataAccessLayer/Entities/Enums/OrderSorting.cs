﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
    public enum OrderSorting {
        Id = 0,
        Amount = 1,
        CreationData = 2
    }
}
