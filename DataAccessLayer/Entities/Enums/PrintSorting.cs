﻿
namespace EducationApp.DataAccessLayer.Entities.Enums
{
    public enum PrintSorting
    {
        Id = 0,
        Title = 1,
        Price = 2
    }
}
