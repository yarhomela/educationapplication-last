﻿
namespace EducationApp.DataAccessLayer.Entities
{
    public class AuthorInPrintingEdition : Base.Base
    {
        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }
        public  string PrintingEditionId { get; set; }
        public virtual PrintingEdition PrintingEdition { get; set; }
    }
}
