﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace EducationApp.DataAccessLayer.Entities
{
    public class PrintingEdition : Base.Base
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Currency Currency {get; set; }
        public TypePrintEdit TypePrintEdit { get; set; }
        public virtual OrderItem OrderItem { get; set; }
        public virtual ICollection<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }
    }
}
