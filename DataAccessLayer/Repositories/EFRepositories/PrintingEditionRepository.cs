﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Base;
using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.EFRepositories
{
    public class PrintingEditionRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionRepository<PrintingEdition>
    {
        ApplicationContext _db;
        DbSet<PrintingEdition> _dbSet;
        public PrintingEditionRepository(ApplicationContext context)
            : base(context)
        {
            _db = context;
            _dbSet = context.Set<PrintingEdition>();
        }
        public async Task<GetPrintEditionResponseDTO> GetPrintEditionList(GetPrintEditionRequestDTO requestDTO)
        {
            var printEditionDTO = new GetPrintEditionResponseDTO();
            var printingEditions = _dbSet.AsNoTracking();
            printingEditions = printingEditions.Where(w => w.IsRemoved == false);
            if ( !string.IsNullOrEmpty(requestDTO.SearchByTitle))
            {
                printingEditions = printingEditions.Where(w => w.Title.Contains(requestDTO.SearchByTitle));
            }
            if(requestDTO.MaxPrice == 0)
            {
                requestDTO.MaxPrice = await printingEditions.OrderByDescending(x=> x.Price).Select(x=> x.Price).FirstOrDefaultAsync();
                requestDTO.MinPrice = await printingEditions.OrderBy(x => x.Price).Select(x => x.Price).FirstOrDefaultAsync();
            }
            printEditionDTO.MinPrice = requestDTO.MinPrice;
            printEditionDTO.MaxPrice = requestDTO.MaxPrice;
            printingEditions = printingEditions.Where(w => w.Price >= requestDTO.MinPrice).Where(w => w.Price <= requestDTO.MaxPrice);
            if (requestDTO.TypePrintEdit != TypePrintEdit.None)
            {
                printingEditions = printingEditions.Where(w => w.TypePrintEdit == requestDTO.TypePrintEdit);
            }
            if (requestDTO.PrintSorting == PrintSorting.Price)
            {
                printingEditions = printingEditions.OrderBy(o => o.Price);
            }
            if (requestDTO.PrintSorting == PrintSorting.Title)
            {
                printingEditions = printingEditions.OrderBy(o => o.Title);
            }
            if (requestDTO.PrintSorting == PrintSorting.Id)
            {
                printingEditions = printingEditions.OrderBy(o => o.Id);
            }
            printEditionDTO.TotalItems = printingEditions.Count();
            printEditionDTO.PrintingEditions = new List<PrintingEdition>();
            printEditionDTO.PrintingEditions = await printingEditions.Include(i=> i.AuthorInPrintingEditions).ThenInclude(th=> th.Author)
                .Skip((requestDTO.Page - 1) * requestDTO.PageSize).Take(requestDTO.PageSize).ToListAsync();
            var printEditionsList2 = await printingEditions.Include(i => i.AuthorInPrintingEditions).ThenInclude(th => th.Author)
                .Skip((requestDTO.Page - 1) * requestDTO.PageSize).Take(requestDTO.PageSize).ToListAsync();
            return printEditionDTO;
        }
        public async Task<PrintingEdition> FindWithAuthor(string printId)
        {
            var printEditions = _dbSet.AsNoTracking();
            printEditions = printEditions.Where(w => w.IsRemoved == false).Where(w => printId == w.Id)
                .Include(i => i.AuthorInPrintingEditions).ThenInclude(th => th.Author);
            var printEdition = await printEditions.FirstOrDefaultAsync();
            return printEdition;
        }
        public async Task<bool> UpdateWithAuthor(PrintingEdition item)
        {
            _db.PrintingEditions.Update(item);
            foreach( var authorInPrint in item.AuthorInPrintingEditions)
            {
                _db.AuthorInPrintingEditions.Update(authorInPrint);
            }
            var result = await _db.SaveChangesAsync();
            return (result > 0);
        }
    }
}
