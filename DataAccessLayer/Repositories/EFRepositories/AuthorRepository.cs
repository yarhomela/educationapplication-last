﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Base;
using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorRepository : BaseEFRepository<Author>, IAuthorRepository<Author>
    {
        DbSet<Author> _dbSet;
        public AuthorRepository(ApplicationContext context)
            : base(context)
        {
            _dbSet = context.Set<Author>();
        }
        public async Task<GetAuthorsResponseDTO> GetAuthorsList(int page, int pageSize, AuthorSorting authorSorting)
        {
            var responseDTO = new GetAuthorsResponseDTO();
            var authors = _dbSet.AsNoTracking();
            authors = authors.Where(w => w.IsRemoved == false);
            if (authorSorting == AuthorSorting.Id)
            {
                authors = authors.OrderBy(o => o.Id);
            }
            if (authorSorting == AuthorSorting.Name)
            {
                authors = authors.OrderBy(o => o.Name);
            }
            responseDTO.TotalItems = authors.Count();
            responseDTO.Authors = await authors.Include(i => i.AuthorInPrintingEditions).ThenInclude(th => th.PrintingEdition)
                .Skip((page - 1) * pageSize).Take(pageSize).Select(s => new AuthorDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                    Products = s.AuthorInPrintingEditions.Where(wh => wh.IsRemoved == false).Where(w => w.AuthorId == s.Id).Select(se => se.PrintingEdition.Title).ToList()
                }).ToListAsync();
            return responseDTO;
        }
        public async Task<IList<Author>> GetAllAuthors()
        {
            var authors = _dbSet.AsNoTracking();
            authors = authors.OrderBy(o => o.Name);
            var authorsList = await authors.Where(w => w.IsRemoved == false).ToListAsync();
            return authorsList;
        }
        public async Task<Author> FindByNameAsync(string authorName)
        {
            var authors = _dbSet.AsNoTracking();
            var author = await authors.Where(w => w.Name == authorName).FirstOrDefaultAsync();
            return author;
        }
    }
}
