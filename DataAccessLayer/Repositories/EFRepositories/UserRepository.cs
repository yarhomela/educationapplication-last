﻿using EducationApp.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using EducationApp.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using EducationApp.DataAccessLayer.Entities.Enums;

namespace EducationApp.DataAccessLayer.Repositories.EFRepositories
{
    public class UserRepository : BaseEFRepository<User>, IUserRepository<User>
    {
        UserManager<User> _userManager;
        RoleManager<IdentityRole> _roleManager;
        public UserRepository (ApplicationContext context,UserManager<User> userManager, RoleManager<IdentityRole> roleManager )
            : base (context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }
        public async Task<bool> AddRole(string roleName)
        {
            var role = new IdentityRole { Name = roleName };
            var result = await _roleManager.CreateAsync(role);
            return result.Succeeded;
        }
        public async Task<string> GetRole(User item)
        {
            var roles = await _userManager.GetRolesAsync(item);
            var role = roles.FirstOrDefault();
            return role;
        }
        public async Task<bool> ResetPasword(User item, string newPass) 
        {
            var token = await _userManager.GeneratePasswordResetTokenAsync(item);
            var result = await _userManager.ResetPasswordAsync(item, token, newPass);
            return result.Succeeded;
        }
        public async Task<bool> ConfirmEmail(User item)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(item);
            var result = await _userManager.ConfirmEmailAsync(item, token);
            return result.Succeeded;
        }
        public async Task<(IList<User>, int totalItems)> GetUserList(int page, int pageSize, UserSorting userSorting)
        {
            var users = _userManager.Users.Where(w => w.IsRemoved == false);
            if (userSorting == UserSorting.byName)
            {
                users = users.OrderBy(o => o.FirstName);
            }
            if (userSorting == UserSorting.byEmail)
            {
                users = users.OrderBy(o => o.Email);
            }
            var totalItems = users.Count();
            var userList = await users
                .Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            return (userList,totalItems);
        }
    }
}
