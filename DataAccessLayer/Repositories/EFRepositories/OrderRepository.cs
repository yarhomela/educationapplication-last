﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Base;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderRepository : BaseEFRepository<Order>, IOrderRepository<Order>
    {
        DbSet<Order> _dbSet;
        ApplicationContext _context;
        public OrderRepository(ApplicationContext context)
            : base(context)
        {
            _dbSet = context.Set<Order>();
            _context = context;
        }

        public async Task<bool> CreateWithOrderItems(Order order)
        {
            var isCreated = await CreateAsync(order);
            return isCreated;
        }


        public async Task<(IList<Order>, int totalItems)> GetOrderList(int page, int pageSize, User user, bool isAdmin, Status status, OrderSorting orderSorting)
        {
            var orders = _dbSet.AsNoTracking();
            if (status != Status.None)
            {
                orders = orders.Where(w => w.Status == status);
            }
            if (orderSorting == OrderSorting.Amount)
            {
                orders = orders.OrderByDescending(o => o.Amount);
            }
            if (orderSorting == OrderSorting.Id)
            {
                orders = orders.OrderBy(o => o.Id);
            }
            if (orderSorting == OrderSorting.CreationData)
            {
                orders = orders.OrderByDescending(o => o.CreationData);
            }
            var totalItems = orders.Count();
            orders = orders.Where(w => w.IsRemoved == false);
            if (!isAdmin)
            {
                orders = orders.Where(x => x.UserId == user.Id);
            }
            var orderList = await orders.Include(o => o.OrderItems).ThenInclude(th => th.PrintingEdition).Include(o => o.User).Include(i=> i.Payment)
                .Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            return (orderList, 50);
        }
        public async Task<bool> SecureUpdate(Order order)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var isUpdated = await UpdateAsync(order);
                    await transaction.CommitAsync();
                    return isUpdated;
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    return false;
                }
            }
        }
    }

}
