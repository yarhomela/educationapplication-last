﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class PaymentDapperRepository : BaseDapperRepository<Payment>, IPaymentRepository<Payment>
    {
        public PaymentDapperRepository(IConfiguration configuration, string tableName = "Payments")
        : base(configuration,tableName)
        {
        }

    }
}
