﻿using Dapper;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderDapperRepository : BaseDapperRepository<Order>, IOrderRepository<Order>
    {
        string sqlConnectionString = null;
        public OrderDapperRepository(IConfiguration configuration, string tableName = "Orders")
        : base(configuration, tableName)
        {
            sqlConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<(IList<Order>, int totalItems)> GetOrderList(int page, int pageSize, User identityUser, bool isAdmin, Status status, OrderSorting orderSorting)
        {
            IEnumerable<Order> orders;
            int totalItems;
            int skipPages = (page - 1) * pageSize;
            string forUserQuery="";
            if(!isAdmin)
            {
                forUserQuery = $" AND UserId = (SELECT Id FROM AspNetUsers WHERE Id = '{identityUser.Id}') ";
            }
            string sqlQuery = "SELECT* FROM ( " +
                                    " SELECT* FROM Orders WHERE IsRemoved = 0 " +
                                    "AND (SELECT IsRemoved FROM AspNetUsers WHERE Id = Orders.UserId ) = 0 " +
                                    forUserQuery +
                                    $" ORDER BY {orderSorting.ToString()} " +
                                    $" OFFSET {skipPages} ROW FETCH NEXT {pageSize} ROWS ONLY ) AS O " +
                             " INNER JOIN OrderItems AS B ON B.OrderId = O.Id" +
                             " LEFT JOIN AspNetUsers AS U ON O.UserId = U.Id" +
                             " LEFT JOIN PrintingEditions AS P ON B.PrintingEditionId = B.Id" +
                             " LEFT JOIN Payments AS Pa ON Pa.OrderId = O.Id" +
                             " WHERE B.IsRemoved = 0 ";
            string userCheckQuery = "";
            if (!isAdmin)
            {
                userCheckQuery = $" AND O.UserId='{identityUser.Id}'";
                sqlQuery += userCheckQuery;
            }
            if (status != Status.None)
            {
                sqlQuery += $" AND O.Status='{status.GetHashCode()}'";
            }
            string totalItemsQuery = "; SELECT COUNT(*) FROM Orders AS O INNER JOIN AspNetUsers AS U ON O.UserId=U.Id " +
                                        "WHERE O.IsRemoved = 0 AND U.IsRemoved = 0 " + userCheckQuery;
            sqlQuery += totalItemsQuery;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var orderDictionary = new Dictionary<string, Order>();
                var orderItemDictionary = new Dictionary<string, OrderItem>();
                var multi = await connection.QueryMultipleAsync(sqlQuery);
                orders = multi.Read<Order, OrderItem, User, PrintingEdition, Payment, Order>(
                    (order, orderItem, user, PrintingEdition, payment) =>
                    {
                        Order orderEntry;
                        OrderItem orderItemEntry;
                        if (orderItem.PrintingEditionId != null && !orderItemDictionary.TryGetValue(orderItem.PrintingEditionId, out orderItemEntry))
                        {
                            orderItemEntry = orderItem;
                            orderItemEntry.PrintingEdition = PrintingEdition;
                            orderItemDictionary.Add(orderItemEntry.PrintingEditionId, orderItemEntry);
                        }
                        if (!orderDictionary.TryGetValue(order.Id, out orderEntry))
                        {
                            orderEntry = order;
                            orderEntry.OrderItems = new List<OrderItem>();
                            orderEntry.Payment = payment;
                            orderDictionary.Add(orderEntry.Id, orderEntry);
                        }
                        orderEntry.User = user;
                        orderEntry.OrderItems.Add(orderItem);
                        return orderEntry;
                    }
                ).Distinct();
                totalItems = await multi.ReadFirstOrDefaultAsync<int>();
            }
            var orderList = orders.ToList();
            return (orderList, totalItems);
        }
        public async Task<bool> SecureUpdate(Order order)
        {
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                await connection.OpenAsync();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var isUpdated = await UpdateAsync(order);
                        await transaction.CommitAsync();
                        return isUpdated;
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();
                        return false;
                    }
                }

            }
        }
        public async Task<bool> CreateWithOrderItems(Order order)
        {
            string sqlQuery = "INSERT INTO Orders ([Description],[Amount],[UserId],[Status],[Currency],[Id],[IsRemoved],[CreationData])  " +
                "VALUES(@Description,@Amount,@UserId,@Status,@Currency,@Id,@IsRemoved,@CreationData)";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var isSuccess = await connection.ExecuteAsync(sqlQuery, order);
            }
            string createOrderItems = "";
            var orderItems = order.OrderItems.ToList();
            var dateFormat = "yyyy-MM-dd HH:mm:ss:fff";
            var datetime2 = order.CreationData.ToString(dateFormat);
            foreach (var item in orderItems)
            {
                createOrderItems += " INSERT INTO OrderItems ([CreationData],[Id],[IsRemoved],[OrderId],[PrintingEditionId],[Quantity],[TypePrintEdit])" +
                    $"VALUES('{datetime2}','{item.Id}','false','{order.Id}','{item.PrintingEditionId}',{item.Quantity},{item.TypePrintEdit.GetHashCode()});";
            }
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var isSuccess = await connection.ExecuteAsync(createOrderItems);
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
