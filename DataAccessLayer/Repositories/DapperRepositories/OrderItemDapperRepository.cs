﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderItemDapperRepository : BaseDapperRepository<OrderItem>, IOrderItemRepository<OrderItem>
    {
        public OrderItemDapperRepository(IConfiguration configuration, string tableName = "OrderItems")
        : base(configuration,tableName)
        {
        }
    }
}
