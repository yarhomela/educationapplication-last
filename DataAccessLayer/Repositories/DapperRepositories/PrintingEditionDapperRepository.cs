﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class PrintingEditionDapperRepository : BaseDapperRepository<PrintingEdition>, IPrintingEditionRepository<PrintingEdition>
    {
        string sqlConnectionString = null;
        public PrintingEditionDapperRepository(IConfiguration configuration, string tableName = "PrintingEditions")
        : base(configuration,tableName)
        {
            sqlConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PrintingEdition> FindWithAuthor(string printId)
        {
            IEnumerable<PrintingEdition> printingEditions;
            string sqlQuery = "SELECT* FROM PrintingEditions AS P"
                + " INNER JOIN AuthorInPrintingEditions AS AiP ON P.Id = AiP.PrintingEditionId"
                + " INNER JOIN Authors AS A ON A.Id = AiP.AuthorId"
                + $" WHERE P.Id='{printId}'";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var printEditionDictionary = new Dictionary<string, PrintingEdition>();
                printingEditions = await connection.QueryAsync<PrintingEdition, AuthorInPrintingEdition, Author, PrintingEdition>(sqlQuery, 
                    (printEdition,authorInPrint,author)=>
                {
                    authorInPrint.Author = author;
                    printEdition.AuthorInPrintingEditions = new Collection<AuthorInPrintingEdition>();
                    printEdition.AuthorInPrintingEditions.Add(authorInPrint);
                    return printEdition;
                });
            }
            var printEdition = printingEditions.FirstOrDefault();
            return printEdition;
        }
        public async Task<GetPrintEditionResponseDTO> GetPrintEditionList(GetPrintEditionRequestDTO requestDTO)
        {
            var printEditionDTO = new GetPrintEditionResponseDTO();
            IEnumerable<PrintingEdition> printingEditions;
            bool isFirstPage = (requestDTO.Page == 1);
            int skipPages = ((requestDTO.Page - 1) * requestDTO.PageSize);
            string sqlQuery = "SELECT* FROM PrintingEditions AS PE"
                + " INNER JOIN AuthorInPrintingEditions AS AP ON AP.PrintingEditionId = PE.Id"
                + " INNER JOIN Authors AS A ON AP.AuthorId = A.Id"
                + " WHERE PE.IsRemoved='false' AND A.IsRemoved='false'";
            string searchStringQuery = "";
            if (!string.IsNullOrEmpty(requestDTO.SearchByTitle))
            {
                searchStringQuery = $" AND Title LIKE '%{requestDTO.SearchByTitle}%'";
                sqlQuery += searchStringQuery;
            }
            string categoryStringQuery = "";
            if (requestDTO.TypePrintEdit != TypePrintEdit.None)
            {
                categoryStringQuery = $" AND TypePrintEdit='{Convert.ToInt32(requestDTO.TypePrintEdit)}'";
                sqlQuery += categoryStringQuery;
            }
            string priceStringQuery = "";
            if (!isFirstPage)
            {
                priceStringQuery = $" AND PE.Price >= {requestDTO.MinPrice} AND PE.Price <= {requestDTO.MaxPrice}";
                sqlQuery += priceStringQuery;
            }
            var printSortingName = requestDTO.PrintSorting.ToString();
            if (requestDTO.PrintSorting != PrintSorting.Id)
            {
                sqlQuery += $" ORDER BY PE.{printSortingName}";
            }
            if (requestDTO.PrintSorting == PrintSorting.Id)
            {
                sqlQuery += " ORDER BY PE.Id";
            }
            string minPriceQuery = "; SELECT MIN(Price) FROM PrintingEditions WHERE IsRemoved='false' ";
            string maxPriceQuery = " ; SELECT MAX(Price) FROM PrintingEditions WHERE IsRemoved='false'";
            sqlQuery += $" OFFSET {skipPages} ROW FETCH NEXT {requestDTO.PageSize} ROWS ONLY"
                    + " ;SELECT COUNT(*) FROM PrintingEditions AS PE, AuthorInPrintingEditions AS AiP, Authors AS A WHERE PE.IsRemoved = 'false' "
                    + "AND A.IsRemoved = 'false' AND AiP.PrintingEditionId = PE.Id AND AiP.AuthorId = A.Id" + searchStringQuery + categoryStringQuery +
                    priceStringQuery + minPriceQuery + maxPriceQuery;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var printEditionDictionary = new Dictionary<string, PrintingEdition>();
                var multi = await connection.QueryMultipleAsync(sqlQuery);
                printingEditions = multi.Read<PrintingEdition, AuthorInPrintingEdition, Author, PrintingEdition>((printEdition, authorIn, author) => {
                    PrintingEdition printEditionEntries;
                    if (!printEditionDictionary.TryGetValue(printEdition.Id, out printEditionEntries))
                    {
                        printEditionEntries = printEdition;
                        authorIn.Author = author;
                        printEditionDictionary.Add(printEdition.Id, printEditionEntries);
                    }
                    printEditionEntries.AuthorInPrintingEditions = new Collection<AuthorInPrintingEdition>();
                    printEditionEntries.AuthorInPrintingEditions.Add(authorIn);
                    return printEditionEntries;
                });
                printEditionDTO.TotalItems = await multi.ReadFirstOrDefaultAsync<int>();
                if (isFirstPage)
                {
                    requestDTO.MinPrice = await multi.ReadFirstOrDefaultAsync<int>();
                    requestDTO.MaxPrice = await multi.ReadFirstOrDefaultAsync<int>();
                }
            }
            printEditionDTO.MinPrice = requestDTO.MinPrice;
            printEditionDTO.MaxPrice = requestDTO.MaxPrice;
            printEditionDTO.PrintingEditions = new List<PrintingEdition>();
            printEditionDTO.PrintingEditions = printingEditions.ToList();
            return printEditionDTO;
        }

        public async Task<bool> UpdateWithAuthor(PrintingEdition printingEdition)
        {
            string updatePrintQuery = "UPDATE PrintingEditions SET Title = @Title,Description = @Description,Price = @Price,Currency = @Currency," +
                "TypePrintEdit = @TypePrintEdit,IsRemoved = @IsRemoved WHERE Id = @Id";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                await connection.ExecuteAsync(updatePrintQuery, printingEdition);
            }
            AuthorInPrintingEdition authorInPrinting;
            authorInPrinting = printingEdition.AuthorInPrintingEditions.FirstOrDefault();
            string updateAuthorQuery = "UPDATE AuthorInPrintingEditions SET AuthorId=@AuthorId, IsRemoved=@IsRemoved " +
                " WHERE Id=@Id";
            var updateCount = 0;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                updateCount = await connection.ExecuteAsync(updateAuthorQuery, authorInPrinting);
            }
            return updateCount > 0;
        }
    }
}
