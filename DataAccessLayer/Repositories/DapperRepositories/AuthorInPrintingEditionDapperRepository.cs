﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorInPrintingEditionDapperRepository : BaseDapperRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository<AuthorInPrintingEdition>
    {
        public AuthorInPrintingEditionDapperRepository(IConfiguration configuration, string tableName = "AuthorInPrintingEditions")
        : base(configuration,tableName)
        {
        }

    }
}
