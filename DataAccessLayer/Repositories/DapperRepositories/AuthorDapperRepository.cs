﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;
using EducationApp.DataAccessLayer.Repositories.Base;
using EducationApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Dapper;

namespace EducationApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorDapperRepository : BaseDapperRepository<Author>, IAuthorRepository<Author>
    {
        string sqlConnectionString = null;
        public AuthorDapperRepository(IConfiguration configuration, string tableName = "Authors")
        : base(configuration, tableName)
        {
            sqlConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<GetAuthorsResponseDTO> GetAuthorsList(int page, int pageSize, AuthorSorting authorSorting)
        {
            var responseDTO = new GetAuthorsResponseDTO();
            int totalItems;
            IEnumerable<AuthorDTO> authors;
            int skipPages = (page - 1) * pageSize;
            string sqlQuery = "SELECT* FROM( " +
                    "SELECT* FROM Authors " +
                    "WHERE IsRemoved = 0 " +
                    $"ORDER BY {authorSorting.ToString()} " +
                    $"OFFSET {skipPages} ROW FETCH NEXT {pageSize} ROWS ONLY) AS A " +
                "LEFT JOIN AuthorInPrintingEditions AS B ON A.Id = B.AuthorId " +
                "LEFT JOIN PrintingEditions AS P ON B.PrintingEditionId = P.Id" +
                "; SELECT COUNT(*) FROM Authors WHERE IsRemoved='false'";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                var authorDictionary = new Dictionary<string, AuthorDTO>();
                var multi = await connection.QueryMultipleAsync(sqlQuery);
                authors = multi.Read<Author, AuthorInPrintingEdition, PrintingEdition, AuthorDTO>((author, authorInPrint, printEdition) =>
                {
                    AuthorDTO authorDTO;
                    if (!authorDictionary.TryGetValue(author.Id, out authorDTO))
                    {
                        authorDTO = new AuthorDTO();
                        authorDTO.Products = new List<string>();
                        authorDictionary.Add(author.Id, authorDTO);
                    }
                    authorDTO.Id = author.Id;
                    authorDTO.Name = author.Name;
                    if (printEdition != null)
                    {
                        authorDTO.Products.Add(printEdition.Title);
                    }
                    return authorDTO;
                }
                ).Distinct();
                totalItems = await multi.ReadFirstOrDefaultAsync<int>();
            }
            responseDTO.Authors = authors.ToList();
            responseDTO.TotalItems = totalItems;
            return responseDTO;
        }
        public async Task<IList<Author>> GetAllAuthors()
        {
            IEnumerable<Author> authors;
            string query = "SELECT* FROM Authors";
            query += " WHERE IsRemoved='false'";
            query += " ORDER BY Id";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                authors = await connection.QueryAsync<Author>(query);
            }
            var authorList = authors.ToList();
            return authorList;
        }
        public async Task<Author> FindByNameAsync(string authorName)
        {
            Author author;
            string query = $"SELECT* FROM Authors WHERE Name = '{authorName}'";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                author = await connection.QueryFirstOrDefaultAsync<Author>(query);
            }
            return author;
        }
    }
}
