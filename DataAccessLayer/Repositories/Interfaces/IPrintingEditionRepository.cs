﻿using EducationApp.DataAccessLayer.Entities.DTO;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IPrintingEditionRepository<PrintingEdition>
    {
        Task<bool> CreateAsync(PrintingEdition printingEdition);
        Task<bool> RemoveAsync(string id);
        Task<GetPrintEditionResponseDTO> GetPrintEditionList(GetPrintEditionRequestDTO getPrintEditionRequestDTO);
        Task<PrintingEdition> FindWithAuthor(string printId);
        Task<bool> UpdateWithAuthor(PrintingEdition item);
    }
}
