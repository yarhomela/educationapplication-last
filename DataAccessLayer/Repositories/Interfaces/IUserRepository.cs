﻿using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IUserRepository<User>
    {
        public Task<bool> AddRole(string roleName);
        public Task<string> GetRole(User item);
        public Task<bool> ResetPasword(User item, string newPass);
        public Task<bool> ConfirmEmail(User item);
        public Task<(IList<User>, int totalItems)> GetUserList(int page, int pageSize, UserSorting userSorting);
    }
}
