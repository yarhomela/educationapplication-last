﻿using EducationApp.DataAccessLayer.Entities.DTO;
using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorRepository<Author>
    {
        Task<bool> CreateAsync(Author author);
        Task<bool> UpdateAsync(Author author);
        Task<Author> FindByIdAsync(string id);
        Task<bool> RemoveAsync(string id);
        Task<GetAuthorsResponseDTO> GetAuthorsList(int page, int pageSize, AuthorSorting authorSorting);
        Task<IList<Author>> GetAllAuthors();
        Task<Author> FindByNameAsync(string authorName);
    }
}
