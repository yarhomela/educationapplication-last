﻿using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IPaymentRepository<Payment>
    {
        Task<bool> CreateAsync (Payment payment);
        Task<bool> UpdateAsync (Payment payment);
        Task<Payment> FindByIdAsync (string id);
        Task<bool> RemoveAsync (string id);
    }
}
