﻿using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorInPrintingEditionRepository<AuthorInPrintingEdition>
    {
        Task<bool> CreateAsync(AuthorInPrintingEdition authorInPrintingEdition);
        Task<AuthorInPrintingEdition> FindByIdAsync(string id);
        Task<bool> UpdateAsync(AuthorInPrintingEdition authorInPrintingEdition);
    }
}
