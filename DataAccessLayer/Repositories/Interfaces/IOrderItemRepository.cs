﻿using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderItemRepository<OrderItem>
    {
        Task<bool> CreateAsync(OrderItem orderItem);
        Task<bool> UpdateAsync(OrderItem orderItem);
        Task<OrderItem> FindByIdAsync(string id);
        Task<bool> RemoveAsync(string id);
    }
}
