﻿using EducationApp.DataAccessLayer.Entities;
using EducationApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderRepository<Order>
    {
        Task<bool> UpdateAsync(Order order);
        Task<Order> FindByIdAsync(string id);
        Task<bool> RemoveAsync(string id);
        Task<(IList<Order>,int totalItems)> GetOrderList(int page, int pageSize, User user, bool isAdmin, Status status, OrderSorting orderSorting);
        Task<bool> SecureUpdate(Order order);
        Task<bool> CreateWithOrderItems(Order order);
    }
}
