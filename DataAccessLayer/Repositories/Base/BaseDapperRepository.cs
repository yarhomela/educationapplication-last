﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Base
{
    public class BaseDapperRepository<TEntity> where TEntity : class
    {
        private IEnumerable<PropertyInfo> GetProperties => typeof(TEntity).GetProperties().Where(p => !p.GetMethod.IsVirtual).ToArray();
        string sqlConnectionString = null;
        private readonly string _tableName;
        public BaseDapperRepository( IConfiguration configuration, string tableName)
        {
            sqlConnectionString = configuration.GetConnectionString("DefaultConnection");
            _tableName = tableName;
        }
        public async Task<bool> CreateAsync (TEntity item)
        {
            var createdQuery = new StringBuilder($"INSERT INTO {_tableName} ");
            createdQuery.Append("(");
            var properties = GenerateListOfProperties(GetProperties);
            properties.ForEach(prop => { createdQuery.Append($"[{prop}],"); });
            createdQuery
                .Remove(createdQuery.Length - 1, 1)
                .Append(") VALUES (");
            properties.ForEach(prop => { createdQuery.Append($"@{prop},"); });
            createdQuery
                .Remove(createdQuery.Length - 1, 1)
                .Append(")");
            string sqlQuery = createdQuery.ToString();
            var updateCount = 0;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                updateCount = await connection.ExecuteAsync(sqlQuery, item);
            }
            return updateCount > 0;
        }
        public async Task<bool> UpdateAsync(TEntity item)
        {
            var updateQuery = new StringBuilder($"UPDATE {_tableName} SET ");
            var properties = GenerateListOfProperties(GetProperties);
            properties.ForEach(property =>
            {
                if (!property.Equals("Id"))
                {
                    updateQuery.Append($"{property}=@{property},");
                }
            });
            updateQuery.Remove(updateQuery.Length - 1, 1);
            updateQuery.Append(" WHERE Id=@Id");
            string sqlQuery = updateQuery.ToString();
            var updateCount = 0;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                updateCount = await connection.ExecuteAsync(sqlQuery, item);
            }
            return updateCount > 0;
        }
        public async Task<TEntity> FindByIdAsync(string id)
        {
            TEntity item;
            string query = $"SELECT* FROM {_tableName} WHERE Id = '{id}'";
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                item = await connection.QueryFirstOrDefaultAsync<TEntity>(query);
            }
            return item;
        }
        public async Task<bool> RemoveAsync(string itemID)
        {
            string query = $"DELETE FROM {_tableName} WHERE Id='{itemID}'";
            var updateCount = 0;
            using (var connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();  
                updateCount = await connection.ExecuteAsync(query);
            }
            return updateCount > 0;
        }

        private static List<string> GenerateListOfProperties(IEnumerable<PropertyInfo> listOfProperties)
        {
            return (from prop in listOfProperties
                    let attributes = prop.GetCustomAttributes(typeof(DescriptionAttribute), false)
                    where attributes.Length <= 0 || (attributes[0] as DescriptionAttribute)?.Description != "ignore"
                    select prop.Name).ToList();
        }
    }
}

