﻿using EducationApp.DataAccessLayer.Entities.Base;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace EducationApp.DataAccessLayer.Repositories.Base
{
    public class BaseEFRepository<TEntity> where TEntity : class
    {
        ApplicationContext _context;
        DbSet<TEntity> _dbSet;

        public BaseEFRepository (ApplicationContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }
        public async Task<bool> CreateAsync(TEntity item)
        {
            _dbSet.Add(item);
            var result = await _context.SaveChangesAsync();
            return (result > 0);
        }
        public async Task<bool> UpdateAsync(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            var result = await _context.SaveChangesAsync();
            return (result > 0);
        }
        public async Task<TEntity> FindByIdAsync(string id)
        {
            var result = await _dbSet.FindAsync(id);
            return result;
        }
        public async Task<bool> RemoveAsync(string id)
        {
            var item = FindByIdAsync(id);
            if(item==null)
            {
                return false;
            }
            _context.Remove(item);
            var result = await _context.SaveChangesAsync();
            return (result > 0);
        }
    }
}
